//
//  AlphaNumericRule.swift
//  WLI-Base-Swift
//
//  Created by xx on 07/04/17.
//  Copyright © 2017 xx. All rights reserved.
//

import Foundation

/**
 `AlphaNumericRule` is a subclass of `CharacterSetRule`. It is used to verify that a field has a
 valid list of alphanumeric characters.
 */
open class AlphaNumericRule: CharacterSetRule {

    /**
     Initializes a `AlphaNumericRule` object to verify that field has valid set of alphanumeric characters.

     - parameter message: String of error message.
     - returns: An initialized object, or nil if an object could not be created for some reason that would not result in an exception.
     */
    public init(message: String = ErrorManager.alphaNumbericError.errorDescription!) {
        super.init(characterSet: CharacterSet.alphanumerics, message: message)
    }
}
