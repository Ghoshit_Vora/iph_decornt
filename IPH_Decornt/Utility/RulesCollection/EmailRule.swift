//
//  EmailRule.swift
//  WLI-Base-Swift
//
//  Created by xx on 07/04/17.
//  Copyright © 2017 xx. All rights reserved.
//

import Foundation

public enum EmailValidationPattern {

    case simple
    case standard
    case regular

    public var pattern: String {
        switch self {
        case .regular : return "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        case .simple: return "^.+@.+\\..+$"
        case .standard: return "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,})$"
        }
    }
}

/**
 `EmailRule` is a subclass of RegexRule that defines how a email is validated.
 */
open class EmailRule: RegexRule {

    /// Regular express string to be used in validation.
    //    static let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"

    /**
     Initializes an `EmailRule` object to validate an email field.

     - parameter message: String of error message.
     - returns: An initialized object, or nil if an object could not be created for some reason that would not result in an exception.
     */
    public convenience init(rulePattern: EmailValidationPattern = .regular, message: String = ErrorManager.emailError.errorDescription!) {
        self.init(regex: rulePattern.pattern, message: message)
    }
}
