//
//  UserDefaultManager.swift
//  IPH_Suscrip
//
//  Created by ios on 30/06/17.
//  Copyright © 2017 ios. All rights reserved.
//

import Foundation

// MARK: - Use
// Preparation
extension UserDefaults {
    struct Account: BoolUserDefaultable {
        private init() {}

        enum BoolDefaultKey: String {
            case isUserLoggedIn
            case isLocalServer
            case isLoginWithFB
            case isFBUserConfigured
            case isFirstTimeApp
            case isStartAssement
            case isVideoPlay
        }
    }

    struct UserData: ObjectUserDefaultable {
        private init() {}
        
        enum ObjectDefaultKey: String {
            case userDetail
            case userId
            case name
            case email
            case phone
            case image
            case cartCount
        }
    }

    struct AppData: ObjectUserDefaultable {
        private init() {}
        enum ObjectDefaultKey: String {
            case deviceToken
            case accessToken
            case date
        }
    }
}
