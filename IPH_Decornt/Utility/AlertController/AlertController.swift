//
//  AlertController.swift
//  WLISwiftLibrary
//
//  Created by ios on 07/04/17.
//  Copyright © 2017 ios. All rights reserved.
//

import UIKit

extension UIViewController {

    // MARK: - Alert Control

    /// Default Alert with Title only
    ///
    /// - Parameter title: Title

    public func showAlertWithTitle(_ title: String?) {
        showAlert(title, message: nil, cancelButtonTitle: "OK")
    }

    /// Default Alert with Blank Title
    ///
    /// - Parameter message: Message which need to be displayed
    public func showAlertWithMessage(_ message: String?) {
        showAlert("", message: message, cancelButtonTitle: "OK")
    }

    /// Default Alert with Title and Message
    ///
    /// - Parameters:
    ///   - title: Title for the Alert
    ///   - message: Message for the Alert
    public func showAlert(_ title: String?, message: String?) {
        showAlert(title, message: message, cancelButtonTitle: "OK")
    }

    /// Default Alert with Cancel Button
    ///
    /// - Parameters:
    ///   - title: Title for the Alert
    ///   - message: Message for the Alert
    ///   - cancelButtonTitle: Cancel for the Alert
    public func showAlert(_ title: String?, message: String?, cancelButtonTitle: String) {
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: nil)
        showAlert(title, message: message, alertActions: [cancelAction])
    }

    /// Default Alert with the Title, Message and Array of AlertAction
    ///
    /// - Parameters:
    ///   - title: Title for the Alert
    ///   - message: Message for the Alert
    ///   - alertActions: Action for the AlertController
    public func showAlert(_ title: String?, message: String?, alertActions: [UIAlertAction]) {
        showAlert(title, message: message, preferredStyle: .alert, alertActions: alertActions)
    }

    // MARK: - ActionSheet

    /// Action sheet with Title
    ///
    /// - Parameter title: Title for the Action
    public func showActionSheetWithTitle(_ title: String?) {
        showActionSheet(title, message: nil, cancelButtonTitle: "OK")
    }

    /// ActionSheet with the Message
    ///
    /// - Parameter message: Message for the Action
    public func showActionSheetWithMessage(_ message: String?) {
        showActionSheet(nil, message: message, cancelButtonTitle: "OK")
    }

    /// ActionSheet with Title and Message
    ///
    /// - Parameters:
    ///   - title: Title for the Action
    ///   - message: Message for the Action
    public func showActionSheet(_ title: String?, message: String?) {
        showActionSheet(title, message: message, cancelButtonTitle: "OK")
    }

    /// ActionSheet with Title,Message and Cancel Button
    ///
    /// - Parameters:
    ///   - title: Title for the ActionSheet
    ///   - message: Message for the ActionSheet
    ///   - cancelButtonTitle: Cancel for the ActionSheet
    public func showActionSheet(_ title: String?, message: String?, cancelButtonTitle: String) {
        let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: nil)
        showActionSheet(title, message: message, alertActions: [cancelAction])
    }

    /// ActionSheet with Title,Message and ActionSheets
    ///
    /// - Parameters:
    ///   - title: Title for the ActionSheet
    ///   - message: Message for the ActionSheet
    ///   - alertActions: Actions for the ActionSheet
    public func showActionSheet(_ title: String?, message: String?, alertActions: [UIAlertAction]) {
        showAlert(title, message: message, preferredStyle: .actionSheet, alertActions: alertActions)
    }

    // MARK: - Common

    /// ActionSheet with Title,Message, PreferredStyle and AlertActions
    ///
    /// - Parameters:
    ///   - title: Title for the
    ///   - message: Message Passed from the revalant method for showing in the Alert or Action Sheet
    ///   - preferredStyle: Actionsheet or alert
    ///   - alertActions: Array of Action which are need to be executed
    func showAlert(_ title: String?, message: String?, preferredStyle: UIAlertControllerStyle, alertActions: [UIAlertAction]) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)

        for alertAction in alertActions {
            alertController.addAction(alertAction)
        }

        self.present(alertController, animated: true, completion: nil)
    }
}
