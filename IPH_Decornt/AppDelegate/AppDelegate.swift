//
//  AppDelegate.swift
//  IPH_Decornt
//
//  Created by xx on 17/08/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit
import SJSwiftSideMenuController
import IQKeyboardManagerSwift
import SystemConfiguration

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    static let sharedInstance = UIApplication.shared.delegate as! AppDelegate

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        
        if UserDefaults.Account.bool(forKey: UserDefaults.Account.BoolDefaultKey.isUserLoggedIn) {
            
            let vc = AppStoryboard.Dashboard.viewController(viewControllerClass: DashboardVC.self)
            AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: vc)
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate {
    
    func SetUpViewInSideControllerView(rootVC:UIViewController) {
        
        let sideMenuVC_Left = AppStoryboard.Dashboard.viewController(viewControllerClass: MenuVC.self)
        SJSwiftSideMenuController.setUpNavigation(rootController: rootVC, leftMenuController: sideMenuVC_Left, rightMenuController: nil, leftMenuType: .SlideOver, rightMenuType: .SlideView)
        
        SJSwiftSideMenuController.leftMenuWidth = 250
        SJSwiftSideMenuController.enableDimbackground = true
        SJSwiftSideMenuController.enableSwipeGestureWithMenuSide(menuSide: .NONE)
        self.window?.rootViewController = AppStoryboard.Dashboard.initialViewController()
   }
    
    //---------------------------------------------------------------------------
    
    func startLoadingIndicator(_ message: String = "Please wait..") {
        DispatchQueue.main.async {
            let actData = WLILoadingIndicatorProperties(message: message, type: IndicatorType.ballClipRotate, indicatorColor: .red, textColor: UIColor.white)
            WLILoadingIndicator.sharedManager.startAnimating(actData)
        }
    }
    
    //---------------------------------------------------------------------------
    
    func stopLoadingIndicator() {
        DispatchQueue.main.async {
            WLILoadingIndicator.sharedManager.stopAnimating()
        }
    }
    
    //---------------------------------------------------------------------------
    
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        let isReachable = flags == .reachable
        let needsConnection = flags == .connectionRequired
        
        return isReachable && !needsConnection
    }
    
    //---------------------------------------------------------------------------
    
    func setStatusBarBackgroundColor(color: UIColor) {
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = color
    }
}
