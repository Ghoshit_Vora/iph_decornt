//
//  BaseVC.swift
//  IPH_Decornt
//
//  Created by xx on 21/08/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit
import SJSwiftSideMenuController

class BaseVC: UIViewController {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Navigation Methods
    
    //---------------------------------------------
    
    func setTitle(title : String) {
        
        self.navigationController?.navigationBar.barTintColor = UIColor.mainTopNavigationColor
        
     self.title = title
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
    }
    
    func setLeftBar(isMenuRequired : Bool) {
        
        if isMenuRequired {
            let newBtn = UIBarButtonItem(image: UIImage(named: "imgMenuIcon"), style: .plain, target: self, action: #selector(btnMenuTapped))
            
            self.navigationController!.navigationBar.tintColor = UIColor.white
            self.navigationItem.leftBarButtonItem = newBtn
        } else {
            let newBtn = UIBarButtonItem(image:#imageLiteral(resourceName: "imgBackWhite"), style: .plain, target: self, action: #selector(btnBackTapped))
            
            self.navigationController!.navigationBar.tintColor = UIColor.white
            self.navigationItem.leftBarButtonItem = newBtn
        }
    }
    
    func setRightData(isSearchBtnRequired : Bool)  {
        
        let label = UILabel(frame: CGRect(x: 15, y: -15, width: 20, height: 20))
        label.layer.borderColor = UIColor.clear.cgColor
        label.layer.borderWidth = 2
        label.layer.cornerRadius = label.bounds.size.height / 2
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.font = UIFont(name: "maven-pro.regular", size: 13)
        label.textColor = .white
        label.backgroundColor = UIColor(red: 185/255.0, green: 0/255.0, blue: 66/255.0, alpha: 1.0)
        if let cartCount = UserDefaults.UserData.object(forKey: .cartCount) as? String {
            label.text = cartCount
        } else {
            label.text = "0"
        }
        
        
        
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 18, height: 16))
        rightButton.setBackgroundImage(UIImage(named: "imgShoppingCart"), for: .normal)
        rightButton.addTarget(self, action: #selector(btnShoppingCartTapped), for: .touchUpInside)
        rightButton.addSubview(label)
        //let shoppingBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "imgShoppingCart"), style: .plain, target: self, action: #selector(btnShoppingCartTapped))
        
        let shoppingBtn = UIBarButtonItem(customView: rightButton)
        let searchBtn = UIBarButtonItem(image: #imageLiteral(resourceName: "imgSearch"), style: .plain, target: self, action: #selector(btnSearchTapped))
        
        self.navigationController!.navigationBar.tintColor = UIColor.white
        
        if isSearchBtnRequired {
            self.navigationItem.rightBarButtonItems = [shoppingBtn, searchBtn]
        } else {
            self.navigationItem.rightBarButtonItems = [shoppingBtn]
        }
        
    }
    
    //---------------------------------------------
    
    //MARK:- Action Methods
    
    //---------------------------------------------
    
    @objc func btnMenuTapped() {
        
        SJSwiftSideMenuController.toggleLeftSideMenu()
    }
    
    @objc func btnBackTapped() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnShoppingCartTapped() {
        
        let myCartVC = AppStoryboard.MyCart.viewController(viewControllerClass: MyCartVC.self)
        
        self.navigationController?.pushViewController(myCartVC, animated: true)
    }
    
    @objc func btnSearchTapped() {
        
        
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}
