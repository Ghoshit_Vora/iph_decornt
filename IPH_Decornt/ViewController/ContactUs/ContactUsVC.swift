//
//  ContactUsVC.swift
//  IPH_Decornt
//
//  Created by xx on 16/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class ContactUsVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var tvMessage: UITextView!
    @IBOutlet weak var lblLine1: UILabel!
    @IBOutlet weak var lblLine2: UILabel!
    @IBOutlet weak var lblLine4: UILabel!
    @IBOutlet weak var lblLine5: UILabel!
    @IBOutlet weak var btnPhone: UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    
    @IBOutlet weak var viewContact: UIView!
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Methods
    
    //---------------------------------------------
    
    func setUpView() {
        
        self.setTitle(title: "Contact Us")
        
        self.setLeftBar(isMenuRequired: true)

        self.viewContact.isHidden = true
        
        self.callContactWS()
    }
    
    //---------------------------------------------
    
    //MARK:- Action Methods
    
    //---------------------------------------------
    
    @IBAction func btnPhoneTapped(_ sender: Any) {
        
        let strNumber = self.btnPhone.titleLabel?.text?.components(separatedBy: CharacterSet.decimalDigits.inverted)
            .joined()
        let strUrl = "tel://" + strNumber!
        
        if let url = URL(string: strUrl), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    
    @IBAction func btnEmailTapped(_ sender: Any) {
        
        let googleUrlString = "googlegmail:///co?to=\(self.btnEmail.titleLabel?.text! ?? "")"
        if let googleUrl = URL(string: googleUrlString) {
            // show alert to choose app
            if UIApplication.shared.canOpenURL(googleUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(googleUrl, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(googleUrl)
                }
            }
        }
    }
    
    @IBAction func btnSubmitTapped(_ sender: Any) {
        
        let validator = Validator()
        validator.registerField(self.txtName, rule: [RequiredRule(message: "Please enter name")])
        validator.registerField(self.txtEmail, rule: [RequiredRule(message: "Please enter email address"), EmailRule(rulePattern: .regular, message: "Please enter valid email address")])
        validator.registerField(self.txtMobile, rule: [RequiredRule(message: "Please enter mobile number"), PhoneNumberRule(message: "Please enter valid mobile number") ])
        
        
        validator.validate({
            
            if self.tvMessage.text == "" || self.tvMessage.text == String(describing: CharacterSet.whitespacesAndNewlines) {
                self.showAlertWithMessage("Please enter mesaage")
            }
            
            self.callWSToSendHelpMessage()
        }) { (arrValidationError) in
            
            if let firstError = arrValidationError.first {
                self.showAlertWithMessage(firstError.errorMessage)
            }
        }
    }
    
    //---------------------------------------------
    
    //MARK:- WS Methods
    
    //---------------------------------------------
    
    func callContactWS() {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        let apiName = "contact_text"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                let contactData:[String : Any] = response as! [String : Any]
                
                self.lblLine1.text = contactData["line1"] as? String
                self.lblLine2.text = contactData["line2"] as? String
                
                self.lblLine4.text = contactData["line3"] as? String
                
                self.lblLine5.text = contactData["line4"] as? String
                
                self.btnPhone.setTitle(contactData["phone"] as? String, for: .normal)
                
                self.btnEmail.setTitle(contactData["email"] as? String, for: .normal)
                
                self.viewContact.isHidden = false
            }
            
        }
    }
    
    func callWSToSendHelpMessage() {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        
        //https://www.decornt.com/mapp/index.php?view=help&name=virag&email=virag@thedezine.in&contact_no=9510069163&message=test&custID=MTg=&phoneType=iphone
        let apiName = "help&name=\(self.txtName.text!)&email=\(self.txtEmail.text!)&contact_no=\(self.txtMobile.text!)&message=\(self.tvMessage.text!)&custID=\(strCustId)&phoneType=iphone"
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            if success {
                var helpData:[String : Any] = response as! [String : Any]
                
                self.showAlert(App.AppName, message: helpData["message"] as? String)
                
                UserDefaults.UserData.set(self.txtName.text as AnyObject, forKey: UserDefaults.UserData.ObjectDefaultKey.name)
                
                
                
                UserDefaults.UserData.set(self.txtEmail.text as AnyObject, forKey: UserDefaults.UserData.ObjectDefaultKey.email)
                
                UserDefaults.UserData.set(self.txtMobile.text as AnyObject, forKey: UserDefaults.UserData.ObjectDefaultKey.phone)
                
            } else {
                self.showAlert(App.AppName, message: response as? String)
            }
        }
        
    }
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}
