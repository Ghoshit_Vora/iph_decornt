//
//  SearchVC.swift
//  IPH_Decornt
//
//  Created by xx on 28/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class SearchVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var tblView: UITableView!
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var searchOriginalResult = NSArray()
    var searchingResult = NSMutableArray()
    var is_searching : Bool = false
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //---------------------------------------------
    
    //MARK:- Custom Methods
    
    //---------------------------------------------
    
    func setUpView() {
        
        self.setTitle(title: "Search")
        
        
        self.setLeftBar(isMenuRequired: false)
        
        
        let nibName = UINib(nibName: "SearchResultCell", bundle: nil)
        self.tblView .register(nibName, forCellReuseIdentifier: "SearchResultCell")
        
        self.tblView.estimatedRowHeight = 44
        
        self.callSearchWS()
        
    }
    
    //---------------------------------------------
    
    //MARK:- WS Methods
    
    //---------------------------------------------
    
    func callSearchWS() {
        
        //        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
        //
        //            return
        //        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        
        //https://www.decornt.com/mapp/index.php?view=search&custID=MTg=&custPhone=9510069163&phoneType=iphone
        let apiName = "search&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                let searchData:[String : Any] = response as! [String : Any]
                
                
                if let searchList = searchData["search_list"] as? NSArray {
                    self.searchOriginalResult = NSArray(array: searchList)
                    //SetSearchResult(searchList)
                    
                    DispatchQueue.main.async
                        {
                            self.tblView.delegate = self
                            self.tblView.dataSource = self
                            self.tblView.reloadData()
                    }
                }
                
            }
            
        }
        
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}

extension SearchVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.is_searching == true {
            return self.searchingResult.count
        }
        return self.searchOriginalResult.count
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.is_searching == true {
            if self.searchingResult.count > indexPath.row {
                if let data = self.searchingResult.object(at: indexPath.row) as? NSDictionary {
                    if let cell = self.tblView.dequeueReusableCell(withIdentifier: "SearchResultCell") as? SearchResultCell {
                        cell.resultData = data
                        return cell
                    }
                }
                
                
            }
            
        }else{
            if self.searchOriginalResult.count > indexPath.row {
                if let data = self.searchOriginalResult.object(at: indexPath.row) as? NSDictionary {
                    if let cell = self.tblView.dequeueReusableCell(withIdentifier: "SearchResultCell") as? SearchResultCell {
                        cell.resultData = data
                        return cell
                    }
                }
                
                
            }
            
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.view.endEditing(true)
        
        if let cell = tableView.cellForRow(at: indexPath) as? SearchResultCell {
            print(cell.resultData)
            let mustableResult = NSMutableDictionary(dictionary: cell.resultData)
            var idStr = ""
            if let searchID = cell.resultData.object(forKey: "productID") as? String {
                idStr = searchID
            }
            if let tempStr = cell.resultData.object(forKey: "type") as? String {
                if tempStr.caseInsensitiveCompare("product") == .orderedSame {
                    mustableResult.setValue(idStr, forKey: "productID")
                    
                    let objRoot = AppStoryboard.Product.viewController(viewControllerClass: ProductDetailsVC.self)
                    objRoot.productResult = mustableResult
                    self.navigationController?.pushViewController(objRoot, animated: true)
                    
                }else{
                    mustableResult.setValue(idStr, forKey: "catID")
                    let viewController = AppStoryboard.Product.viewController(viewControllerClass: ProductListVC.self)
                    viewController.categoryResultData = mustableResult
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            }
            
        }
    }

}

extension SearchVC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        is_searching = true;
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        is_searching = false;
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        is_searching = false;
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        is_searching = false;
        searchBar.showsCancelButton = false
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        if (searchBar.text?.isEmpty)!{
            searchBar.showsCancelButton = false
            is_searching = false
            self.tblView.reloadData()
        } else {
            searchBar.showsCancelButton = true
            let searchText = searchBar.text
            print(" search text %@ ",searchBar.text ?? "Not Found")
            is_searching = true
            self.searchingResult.removeAllObjects()
            
            for data in self.searchOriginalResult {
                if let tempData = data as? NSDictionary {
                    if let tempStr = tempData.object(forKey: "name") as? String {
                        if tempStr.lowercased().range(of: (searchText?.lowercased())!) != nil {
                            self.searchingResult.add(tempData)
                        }
                    }
                }
            }
            
            self.tblView.reloadData()
        }
    }
    
}
