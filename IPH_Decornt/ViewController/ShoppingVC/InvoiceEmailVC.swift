//
//  InvoiceEmailVC.swift
//  IPH_Decornt
//
//  Created by xx on 01/10/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

protocol changeEmail {
    func setEmail(strEmail : String)
}

class InvoiceEmailVC: UIViewController {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    @IBOutlet weak var txtEmail: UITextField!
    
    var delegate:changeEmail?
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Action Methods
    
    //---------------------------------------------
    
    @IBAction func btnOkTapped(_ sender: Any) {
        
        let validator = Validator()
       
        validator.registerField(self.txtEmail, rule: [RequiredRule(message: "Please enter email address"), EmailRule(rulePattern: .regular, message: "Please enter valid email address")])
        
        
        validator.validate({
            
            
            self.delegate?.setEmail(strEmail: self.txtEmail.text!)
            self.dismiss(animated: true, completion: nil)
        }) { (arrValidationError) in
            
            if let firstError = arrValidationError.first {
                self.showAlertWithMessage(firstError.errorMessage)
            }
        }
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}
