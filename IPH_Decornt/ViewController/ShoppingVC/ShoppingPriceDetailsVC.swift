//
//  ShoppingPriceDetailsVC.swift
//  IPH_Decornt
//
//  Created by xx on 01/10/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class ShoppingPriceDetailsVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var txtCoupn: UITextField!
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var lblDecorntAmount: UILabel!
    @IBOutlet weak var lblSuperWalletAmount: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var lblTotalItem: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblItemPrice: UILabel!
    @IBOutlet weak var lblTotalDecorntPrice: UILabel!
    @IBOutlet weak var lblTotalSuperPrice: UILabel!
    @IBOutlet weak var lblGrandTotal: UILabel!
    @IBOutlet weak var lblBottomPrice: UILabel!
    
    @IBOutlet weak var const_CoupanView_Height: NSLayoutConstraint!
    
    @IBOutlet weak var btnApply: UIButton!
    
    @IBOutlet weak var const_DecorntAmount_Height: NSLayoutConstraint!
    
    @IBOutlet weak var lblTitleDecornt: UILabel!
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var dictpayment:[String : Any] = [:]
    var totalPaybleAmount = Double()
    var decorntAmount = Double()
    var superAmount = Double()
    var shippingAmount = Double()
    var strItemCount = String()
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Methods
    
    //---------------------------------------------
    
    func setUpView() {
        
        self.setTitle(title: "Payments")
        self.setLeftBar(isMenuRequired: false)
        self.const_CoupanView_Height.constant = 40
        self.txtCoupn.isHidden = true
        self.btnApply.isHidden = true
        
        self.const_DecorntAmount_Height.constant = 0
        self.lblTitleDecornt.isHidden = true
        self.lblTotalDecorntPrice.isHidden = true
        
        self.setUpPaymentData()
    }
    
    func setUpPaymentData() {
        
        if let shipping = self.dictpayment["shipping"] as? String {
            self.lblItemPrice.text = "+ ₹ \(shipping)"
            self.shippingAmount = Double(shipping)!
        }
        
        if let decorntWallet = self.dictpayment["wallet"] as? String {
            self.lblDecorntAmount.text = "₹ \(decorntWallet)"
            self.lblTotalDecorntPrice.text = "- ₹ \(decorntWallet)"
            self.decorntAmount = Double(decorntWallet)!
        }
        
        if let superWallet = self.dictpayment["user_super_wallet"] as? String {
            self.lblSuperWalletAmount.text = "₹ \(superWallet)"
            self.lblTotalSuperPrice.text = "- ₹ \(superWallet)"
        }
        
        if let superDedcatedWallet = self.dictpayment["super_use_amount"] as? String {
            self.superAmount = Double(superDedcatedWallet)!
        }
        
        self.lblTotalItem.text = self.strItemCount
    }
    
    //---------------------------------------------
    
    //MARK:- Action Method
    
    //---------------------------------------------
    
    @IBAction func btnApplyTapped(_ sender: Any) {
    }
    
    @IBAction func btnDropDownTapped(_ sender: Any) {
    }
    
    //---------------------------------------------
    
    //MARK:- WS Methods
    
    //---------------------------------------------
    
    func callAddressWS() {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        //https://www.decornt.com/mapp/index.php?view=discount&coupon_code=DECO16&subtotal=250&custID=Ng==
        let apiName = "view=discount&coupon_code=DECO16&subtotal=250&custID=\(strCustId)"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                let coupanData:[String : Any] = response as! [String : Any]
                
            }
            
        }
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}
