//
//  ShoppingAddressVC.swift
//  IPH_Decornt
//
//  Created by xx on 27/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class ShoppingAddressVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var tblAddressList: UITableView!
    @IBOutlet weak var lblTotalPrice: UILabel!
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var strItemCount = String()
    var strPrice = String()
    var arrAddressList:[[String:Any]] = []
    var selectedAddressList:[String:Any] = [:]
    var strAddressId = String()
    var isFromChangesAddress = Bool()
    var lblEmail = String()
    var totalAmount = Double()
    var totalItem = Int()
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Methods
    
    //---------------------------------------------
    
    func setUpView() {
        
        self.setTitle(title: "Delivery")
        self.setLeftBar(isMenuRequired: false)
        self.lblTotalPrice.text = self.strPrice
    }
    
    //---------------------------------------------
    
    //MARK:- Action Methods
    
    //---------------------------------------------
    
    @objc func btnChangeAddress() {
        let changeAddressVC = AppStoryboard.MyCart.viewController(viewControllerClass: ShoppingAddressListVC.self)
       
        if let addressId = selectedAddressList["addID"] as? String {
            changeAddressVC.selectedAddressId = addressId
        }
       
       changeAddressVC.arrAddressList = self.arrAddressList
       changeAddressVC.delegate = self
        self.navigationController?.pushViewController(changeAddressVC, animated: true)
    }
    
    @objc func btnAddAddress() {
        let addAddAddressVC = AppStoryboard.Authotication.viewController(viewControllerClass: AddMyAddressVC.self)
        self.navigationController?.pushViewController(addAddAddressVC, animated: true)
    }
    
    @objc func btnChangeEmail() {
        let changeEmailVC = AppStoryboard.MyCart.viewController(viewControllerClass: InvoiceEmailVC.self)
        
        changeEmailVC.modalPresentationStyle = .overCurrentContext
        changeEmailVC.modalTransitionStyle = .crossDissolve
        self.present(changeEmailVC, animated: true, completion: nil)
    }
    
    @IBAction func btnContinueTapped(_ sender: Any) {
        self.callShippingWS()
    }
    
    //---------------------------------------------
    
    //MARK:- WS Methods
    
    //---------------------------------------------
    
    func callAddressWS() {
        
//        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
//
//            return
        //}
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        
        
        //https://www.decornt.com/mapp/index.php?view=addresses&page=list&custID=MTg=&custPhone=9510069163&phoneType=iphone
        let apiName = "addresses&page=list&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                let addressData:[String : Any] = response as! [String : Any]
                
                if let email = addressData["email"] as? String {
                    self.lblEmail = email
                } else {
                    self.lblEmail = ""
                }
                
                
                if let flag = addressData["flag"] as? String {
                    if flag != "no_address" {
                        self.arrAddressList = addressData["address_list"] as! [[String : Any]]
                        
                        if self.arrAddressList.count > 0 {
                            if !self.isFromChangesAddress {
                                self.selectedAddressList = self.arrAddressList[0]
                            }
                            
                        }
                        
                        self.tblAddressList.delegate = self
                        self.tblAddressList.dataSource = self
                        self.tblAddressList.reloadData()
                    } else {
                        let notDeliveryVC = AppStoryboard.MyCart.viewController(viewControllerClass: DeliveryNotAvailbleVC.self)
                        
                        notDeliveryVC.modalPresentationStyle = .overCurrentContext
                        notDeliveryVC.modalTransitionStyle = .crossDissolve
                        self.present(notDeliveryVC, animated: true, completion: nil)
                    }
                } else {
                
                    let notDeliveryVC = AppStoryboard.MyCart.viewController(viewControllerClass: DeliveryNotAvailbleVC.self)
                    
                    notDeliveryVC.modalPresentationStyle = .overCurrentContext
                    notDeliveryVC.modalTransitionStyle = .crossDissolve
                    self.present(notDeliveryVC, animated: true, completion: nil)
                }
            }
            
        }
    }
    
    func callShippingWS() {
        
//        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
//
//            return
//        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        
        
        //https://www.decornt.com/mapp/index.php?view=orders&page=shipping&addressID=166&items=5&subtotal=123&custID=MTg=&custPhone=9510069163&phoneType=iphone
        let apiName = "orders&page=shipping&addressID=\(strAddressId)&items=\(self.arrAddressList.count)&subtotal=\(Int(self.totalAmount))&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                let shppingData:[String : Any] = response as! [String : Any]
                
                let priceDetailsVC = AppStoryboard.MyCart.viewController(viewControllerClass: ShoppingPriceDetailsVC.self)
                
                priceDetailsVC.totalPaybleAmount = self.totalAmount
                priceDetailsVC.strItemCount = self.strItemCount
                priceDetailsVC.dictpayment = shppingData
                self.navigationController?.pushViewController(priceDetailsVC, animated: true)
            } else {
                self.showAlert(App.AppName, message: "Delivery Not Available.")
            }
        }
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        self.isFromChangesAddress = false
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.callAddressWS()
    }
    
    //---------------------------------------------

}

extension ShoppingAddressVC : UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomShoppingAddressCell") as! CustomShoppingAddressCell
            cell.btnChangeAddress.addTarget(self, action: #selector(btnChangeAddress), for: .touchUpInside)
            cell.btnAddAddress.addTarget(self, action: #selector(btnAddAddress), for: .touchUpInside)
            
            cell.setData(dict: selectedAddressList)
            
            if let addressId = selectedAddressList["addID"] as? String {
                self.strAddressId = addressId
            }
            
            return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomInvoiceCell") as! CustomInvoiceCell
            
            cell.lblEmail.text = self.lblEmail
            
            if self.lblEmail == "" {
                cell.btnEditEmail.setTitle("Add Email ?", for: .normal)
            } else {
                cell.btnEditEmail.setTitle("Edit Email", for: .normal)
            }
            
            cell.btnEditEmail.addTarget(self, action: #selector(btnChangeEmail), for: .touchUpInside)
            
            return cell
            
        } else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomPriceDetailCell") as! CustomPriceDetailCell
            
            cell.lblPayblePrice.text = strPrice
            cell.lblPrice.text = strPrice
            cell.lblTotalItem.text = strItemCount
            
            return cell
        }
        
        return UITableViewCell()
    }
}

extension ShoppingAddressVC : selectDeliveryAdderess {
    func seletedAddress(dict: [String : Any]) {
        
        self.selectedAddressList = dict
        self.isFromChangesAddress = true
        
        if let addressId = dict["addID"] as? String {
            self.strAddressId = addressId
        }
        //self.tblAddressList.reloadData()
    }
}

extension ShoppingAddressVC : changeEmail {
    func setEmail(strEmail: String) {
        self.lblEmail = strEmail
        
        self.tblAddressList.reloadData()
    }
    
    
}
