//
//  ShoppingAddressListVC.swift
//  IPH_Decornt
//
//  Created by xx on 01/10/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

protocol selectDeliveryAdderess {
    
    func seletedAddress(dict : [String :Any])
}

class ShoppingAddressListVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var tblAddressList: UITableView!
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var arrAddressList:[[String:Any]] = []
    var selectedAddressId = String()
    var selectedAddressList:[String:Any] = [:]
    var delegate:selectDeliveryAdderess?
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Methods
    
    //---------------------------------------------
    
    func setUpView() {
        
        self.setTitle(title: "Delivery")
        self.setLeftBar(isMenuRequired: false)
        
        self.tblAddressList.delegate = self
        self.tblAddressList.dataSource = self
    }
    
    //---------------------------------------------
    
    //MARK:- Action Methods
    
    //---------------------------------------------
    
    @objc func btnEditTapped(sender : UIButton) {
        
        let dict = self.arrAddressList[sender.tag]
        
        let addAddAddressVC = AppStoryboard.Authotication.viewController(viewControllerClass: AddMyAddressVC.self)
        
        addAddAddressVC.isFromEdit = true
        
        addAddAddressVC.dictData = dict
    self.navigationController?.pushViewController(addAddAddressVC, animated: true)
    }
    
    @objc func btnRadioTapped(sender : UIButton) {
        
        let dict = self.arrAddressList[sender.tag]
        
        if let addressId = dict["addID"] as? String {
            self.selectedAddressId = addressId
            self.selectedAddressList = dict
            self.tblAddressList.delegate = self
            self.tblAddressList.dataSource = self
            self.tblAddressList.reloadData()
        }
    }
    
    
    @IBAction func btnDeliveryHereTapped(_ sender: Any) {
    
        self.delegate?.seletedAddress(dict: selectedAddressList)
        self.navigationController?.popViewController(animated: true)
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}

extension ShoppingAddressListVC : UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrAddressList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomDeliveryAddressCell") as! CustomDeliveryAddressCell
        
        let dict = self.arrAddressList[indexPath.row]
            
        cell.setData(dict: dict, seletedAddressId: self.selectedAddressId)
        
        cell.btnRadio.tag = indexPath.row
        cell.btnEdit.tag = indexPath.row
        
        cell.btnRadio.addTarget(self, action: #selector(btnRadioTapped), for: .touchUpInside)
        
        cell.btnEdit.addTarget(self, action: #selector(btnEditTapped), for: .touchUpInside)
        
        return cell

    }
}
