//
//  CategorySectionHeader.swift
//  TrendOnTap
//
//  Created by Desap Team on 30/01/2018.
//  Copyright © 2018 com.technetapp.trendontap. All rights reserved.
//

import UIKit

protocol CategorySectionHeaderDelegate {
    func didPressSection(header : CategorySectionHeader)
    func didSelectSectionCategoryClick(data: NSDictionary)
}

class CategorySectionHeader: UIView {

    var delegate : CategorySectionHeaderDelegate?
    
    @IBOutlet var imageview : UIImageView!
    @IBOutlet var titleLabel : UILabel!
    
    @IBOutlet var expandButton : UIButton?
    
    var resultData = NSDictionary() {
        didSet{
            self.SetResultUI()
        }
    }
    
    class func instanceFromNib() -> CategorySectionHeader  {
        return UINib(nibName: "CategorySectionHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CategorySectionHeader
        
    }
    @objc func SetResultUI(){
        
        if self.imageview != nil  {
            if let tempStr = resultData.object(forKey: "icon") as? String {
                if tempStr.isEmpty == false {
                    let imageUrl = URL(string:tempStr)
                    self.imageview.sd_setImage(with: imageUrl, placeholderImage: UIImage.init())
                    
                }
            }
            
            if let tempStr = resultData.object(forKey: "name") as? String {
                self.titleLabel.text = tempStr
            }
            
            if let isExpand = resultData.object(forKey: "isExpand") as? String {
                if isExpand.caseInsensitiveCompare("true") == .orderedSame {
                    self.expandButton?.setImage(#imageLiteral(resourceName: "imgMinusBtn"), for: .normal)
                }else{
                    
                    if let isSubCatData = resultData.object(forKey: "subcat") as? String {
                        if isSubCatData.caseInsensitiveCompare("Yes") == .orderedSame {
                            self.expandButton?.setImage(#imageLiteral(resourceName: "imgPlusBtn"), for: .normal)
                        }
                    
                    }
                    
                }
            }
            
        }else{
            self.perform(#selector(CategorySectionHeader.SetResultUI))
        }
        
    }

    @IBAction func touchButtonAction(){
        
        if let isSubCatData = resultData.object(forKey: "subcat") as? String {
            if isSubCatData.caseInsensitiveCompare("Yes") == .orderedSame {
                if self.delegate != nil {
                    self.delegate?.didPressSection(header: self)
                }
                
            } else {
               self.delegate?.didSelectSectionCategoryClick(data: resultData)
            }
        }
        
       
    }
    override func awakeFromNib() {
        
    }
}
