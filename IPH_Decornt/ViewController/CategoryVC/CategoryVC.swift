//
//  CategoryVC.swift
//  IPH_Decornt
//
//  Created by xx on 26/08/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class CategoryVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var tblCategory: UITableView!
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var CategoriesListResul = NSMutableArray()
    var homeCategoryResult = NSDictionary()
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Method
    
    //---------------------------------------------
    
    func setupView() {
        
        
        self.setTitle(title: "Categories")
        self.setLeftBar(isMenuRequired: true)
        
        let nibName = UINib(nibName: "CategoriesListCell", bundle: nil)
        self.tblCategory .register(nibName, forCellReuseIdentifier: "CategoriesListCell")
        
        self.GetProductCategories()
        
    }
    
    //---------------------------------------------
    
    //MARK:- WS Methods
    
    //---------------------------------------------
    
    func GetProductCategories() {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        //https://www.decornt.com/mapp/index.php?view=category1&custID=MTg=&custPhone=9510069163&phoneType=iphone
        let apiName = "category1&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                var categoryData:[String : Any] = response as! [String : Any]
                
                if let resultList = categoryData["category_list_new"] as? NSArray {
                    
                    DispatchQueue.main.async
                        {
                            self.CategoriesListResul = NSMutableArray()
                            for modify in resultList {
                                if let modifyData = modify as? NSDictionary {
                                    let mutableData = NSMutableDictionary(dictionary: modifyData)
                                    mutableData.setObject("false", forKey: "isExpand" as NSCopying)
                                    if let catId = self.homeCategoryResult.object(forKey: "catID") as? String {
                                        if let liveCatId = modifyData.object(forKey: "catID") as? String {
                                            if catId == liveCatId {
                                                mutableData.setObject("true", forKey: "isExpand" as NSCopying)
                                            }
                                        }
                                    }
                                    
                                    self.CategoriesListResul.add(mutableData)
                                }
                            }
                            self.tblCategory.delegate = self
                            self.tblCategory.dataSource = self
                            self.tblCategory.reloadData()
                    }
                }
                
            } else {
                self.showAlert(App.AppName, message: response as? String)
            }
        }
        
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}

extension CategoryVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.CategoriesListResul.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = CategorySectionHeader.instanceFromNib()
        header.delegate = self
        if self.CategoriesListResul.count > section {
            if let data = self.CategoriesListResul.object(at: section) as? NSDictionary {
                header.resultData = data
            }
        }
        return header
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 64
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.CategoriesListResul.count > section {
            if let data = self.CategoriesListResul.object(at: section) as? NSDictionary {
                if let isExpand = data.object(forKey: "isExpand") as? String {
                    //ExpandCell
                    if isExpand.caseInsensitiveCompare("true") == .orderedSame {
                        if let listOfOrder = data.object(forKey: "subcat_list") as? NSArray {
                            
                            let count = Float(listOfOrder.count) / Float(2)
                            return Int(ceil(Double(count)))
                        }
                    }
                }
                
            }
        }
        return  0
        
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 70
//    }
//    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 70
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.CategoriesListResul.count > indexPath.section {
            if let data = self.CategoriesListResul.object(at: indexPath.section) as? NSDictionary {
                if let listOfOrder = data.object(forKey: "subcat_list") as? NSArray {
                    
                    var plusIndex = indexPath.row + indexPath.row
                    if plusIndex == 1{
                        plusIndex = 0
                    }
                    if listOfOrder.count > indexPath.row {
                        if let data = listOfOrder.object(at: plusIndex) as? NSDictionary {
                            if let cell = self.tblCategory.dequeueReusableCell(withIdentifier: "CategoriesListCell") as? CategoriesListCell {
                                cell.delegate = self
                                cell.resultData = data
                                
                                let secondLavelIndex = plusIndex + 1
                                if listOfOrder.count > secondLavelIndex {
                                    if let data2 = listOfOrder.object(at: secondLavelIndex) as? NSDictionary {
                                        cell.resultData2 = data2
                                    }
                                    cell.view2?.isHidden = false
                                }else {
                                    cell.view2?.isHidden = true
                                }
                                
                                return cell
                            }
                        }
                        
                    }
                }
            }
        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

extension CategoryVC : CategorySectionHeaderDelegate,CategoriesListCellDelegate {
    
    func CollsPanAllCells(){
        
        for (index, element) in self.CategoriesListResul.enumerated() {
            if let orderData = element as? NSDictionary {
                if let isExpand = orderData.object(forKey: "isExpand") as? String {
                    
                    //ExpandCell
                    if isExpand.caseInsensitiveCompare("true") == .orderedSame {
                        let mutableOrderData = NSMutableDictionary(dictionary: orderData)
                        mutableOrderData.setValue("false", forKey: "isExpand")
                        
                        self.CategoriesListResul.replaceObject(at: index, with: mutableOrderData)
                        
                    }
                }
                
                
            }
        }
    }

    
    func didSelectCategoryClick(data: NSDictionary) {
        let objRoot = AppStoryboard.Product.viewController(viewControllerClass: ProductListVC.self)
        objRoot.categoryResultData = data
        self.navigationController?.pushViewController(objRoot, animated: true)
    }
    
    func didSelectSectionCategoryClick(data: NSDictionary) {
        let objRoot = AppStoryboard.Product.viewController(viewControllerClass: ProductListVC.self)
        objRoot.categoryResultData = data
        self.navigationController?.pushViewController(objRoot, animated: true)
    }
    
    func didPressSection(header: CategorySectionHeader) {
        if let categoryID = header.resultData.object(forKey: "catID") as? String {
            if let isExpand = header.resultData.object(forKey: "isExpand") as? String {
                if isExpand.caseInsensitiveCompare("true") == .orderedSame {
                    header.expandButton?.setImage(#imageLiteral(resourceName: "imgPlusBtn"), for: .normal)
                    self.CollsPanAllCells()
                }else{
                    self.CollsPanAllCells()
                    let mutableOrderData = NSMutableDictionary(dictionary: header.resultData)
                    mutableOrderData.setValue("true", forKey: "isExpand")
                    header.expandButton?.setImage(#imageLiteral(resourceName: "imgMinusBtn"), for: .normal)
                    //Expand Cell
                    for (index, element) in self.CategoriesListResul.enumerated() {
                        if let orderData = element as? NSDictionary {
                            if let catId = orderData.object(forKey: "catID") as? String {
                                
                                //ExpandCell
                                if catId.caseInsensitiveCompare(categoryID) == .orderedSame {
                                    
                                    self.CategoriesListResul.replaceObject(at: index, with: mutableOrderData)
                                    
                                }
                            }
                            
                        }
                    }
                }
            }
        }
        
        
        DispatchQueue.main.async {
            self.tblCategory.reloadData()
        }
    }
}
