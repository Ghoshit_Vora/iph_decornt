//
//  OrderDetailsCell.swift
//  Sabzilana
//
//  Created by TNM3 on 4/7/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class OrderDetailsCell: UITableViewCell {

    @IBOutlet var nameLabel : UILabel!
    @IBOutlet var qtyLabel : UILabel!
    @IBOutlet var priceLabel : UILabel!
    @IBOutlet var totalLabel : UILabel!
    
    var resultData = NSDictionary() {
        didSet{
            self.SetResultUI()
        }
    }

    
    func SetResultUI(){
        
        if self.nameLabel != nil  {
            
            if let tempStr = resultData.object(forKey: "name") as? String {
                self.nameLabel.text = String(format: ": %@", tempStr)
            }
            if let tempStr = resultData.object(forKey: "quantity") as? String {
                self.qtyLabel.text = String(format: ": %@", tempStr)
            }
            
            if let tempStr = resultData.object(forKey: "price") as? String {
                self.priceLabel.text = String(format: ": ₹ %@", arguments: [tempStr])
            }
            if let tempStr = resultData.object(forKey: "line_total") as? String {
                self.totalLabel.text = String(format: ": ₹ %@", arguments: [tempStr])
            }
            
        }else{
            self.perform(#selector(WalletHistoryCell.SetResultUI))
        }
        
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
