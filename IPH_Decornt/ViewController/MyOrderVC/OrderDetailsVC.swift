//
//  OrderDetailsVC.swift
//  IPH_Decornt
//
//  Created by xx on 28/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class OrderDetailsVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var tblOrderDetails: UITableView!
    @IBOutlet var lblShipping : UILabel!
    @IBOutlet var lblSubTotal : UILabel!
    @IBOutlet var lblGrandTotal : UILabel!
    @IBOutlet var footerView : UIView!
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
     var itemsResul = NSArray()
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Methods
    
    //---------------------------------------------
    
    func setUpView() {
        self.setTitle(title: "Order Detail")
        
        let nibName = UINib(nibName: "OrderDetailsCell", bundle: nil)
        self.tblOrderDetails .register(nibName, forCellReuseIdentifier: "OrderDetailsCell")
        
        self.GetOrderHistory()
    }
    
    //---------------------------------------------
    
    //MARK:- WS Methods
    
    //---------------------------------------------
    
    func GetOrderHistory(){
        
//        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
//
//            return
//        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        //https://www.decornt.com/mapp/index.php?view=orders&page=detail&orderID=1185&custID=MTg=&custPhone=9510069163&phoneType=iphone
        let apiName = "orders&page=detail&orderID=1185&custID=MTg=&custPhone=9510069163&phoneType=iphone"
        
        

        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                var orderData:[String : Any] = response as! [String : Any]
                
                if let resultList = orderData["item_list"] as? NSArray {
                    
                  self.itemsResul = NSArray(array: resultList)
                    
                    if let tempStr = orderData["subtotal"] as? String {
                        self.lblSubTotal.text = String(format: "₹ %@", arguments: [tempStr])
                    }
                    if let tempStr = orderData["grand_total"] as? String {
                        self.lblGrandTotal.text = String(format: "₹ %@", arguments: [tempStr])
                    }
                    if let tempStr = orderData["shipping"] as? String {
                        self.lblShipping.text = String(format: "₹ %@", arguments: [tempStr])
                    }
                    DispatchQueue.main.async
                        {
                            self.tblOrderDetails.delegate = self
                            self.tblOrderDetails.dataSource = self
                            self.tblOrderDetails.reloadData()
                    }
                }
            } else {
                self.showAlert(App.AppName, message: response as? String)
            }
        }
        
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}

extension OrderDetailsVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemsResul.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.itemsResul.count > indexPath.row {
            if let data = self.itemsResul.object(at: indexPath.row) as? NSDictionary {
                if let cell = self.tblOrderDetails.dequeueReusableCell(withIdentifier: "OrderDetailsCell") as? OrderDetailsCell {
                    
                    cell.resultData = data
                    return cell
                }
            }
            
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
}
