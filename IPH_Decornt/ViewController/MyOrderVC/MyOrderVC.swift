//
//  MyOrderVC.swift
//  IPH_Decornt
//
//  Created by xx on 28/08/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class MyOrderVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var tblOrderList: UITableView!
    
    @IBOutlet var viewFooter: UIView!
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var isFromMenu : Bool = true
    var orderResul = NSMutableArray()
    var currentPageCode : Int = 0
    var loadingStatus = LoadMoreStatus.haveMore
    var PAGING_LIMIT : Int = 10
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Methods
    
    //---------------------------------------------
    
    func setUpView() {
        self.setTitle(title: "My Order")
        
//        if isFromMenu {
//            self.setLeftBar(isMenuRequired: true)
//        }
        self.setLeftBar(isMenuRequired: true)
        let nibName = UINib(nibName: "OrderHistoryCell", bundle: nil)
        self.tblOrderList .register(nibName, forCellReuseIdentifier: "OrderHistoryCell")
        //        self.tableview.estimatedRowHeight = 274
//        self.tblOrderList.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0)
        self.GetOrderHistory(pageCode: self.currentPageCode)
    }
    
    @objc func loadMore() {
        //if loadingStatus != .finished {
            self.currentPageCode = self.currentPageCode + 1
            self.GetOrderHistory(pageCode: self.currentPageCode)
            
//        }else{
//            DispatchQueue.main.async {
//                self.tblOrderList.tableFooterView = UIView()
//            }
//        }
        
    }
    
    //---------------------------------------------
    
    //MARK:- WS Methods
    
    //---------------------------------------------
    
    func GetOrderHistory(pageCode : Int){
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        //https://www.decornt.com/mapp/index.php?view=orders&page=list&pagecode=0&custID=MTg=&custPhone=9510069163&phoneType=iphone
        let apiName = "orders&page=list&pagecode=\(pageCode)&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        
        if pageCode <= 0 {
            AppDelegate.sharedInstance.startLoadingIndicator()
            self.orderResul = NSMutableArray()
        }
        
        
        
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                var orderData:[String : Any] = response as! [String : Any]
                
                if let resultList = orderData["order_list"] as? NSArray {
                    
                    for tempData in resultList {
                        self.orderResul.add(tempData)
                    }
                    DispatchQueue.main.async
                        {
                            if pageCode <= 0 {
                                self.tblOrderList.delegate = self
                                self.tblOrderList.dataSource = self
                                self.tblOrderList.reloadData()
                            }else{
                                self.tblOrderList.reloadData()
                            }
                    }
                    
                    self.loadingStatus = .haveMore
                    
                }else{
                    self.loadingStatus = .finished
                    DispatchQueue.main.async
                        {
                            self.tblOrderList.reloadData()
                            
                    }
                }
            } else {
                self.showAlert(App.AppName, message: response as? String)
            }
        }
        
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}

extension MyOrderVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderResul.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 162
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.orderResul.count > indexPath.row {
            if let data = self.orderResul.object(at: indexPath.row) as? NSDictionary {
                if let cell = tableView.dequeueReusableCell(withIdentifier: "OrderHistoryCell") as? OrderHistoryCell {
                
                    
                    
                    
                    cell.resultData = data
                    
                    if self.orderResul.count >= PAGING_LIMIT {
                        if indexPath.row == (self.orderResul.count - 1) {
                            
                            self.loadMore()
                            
                        }
                    }
                    
                    return cell
                }
            }
            
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        if loadingStatus == .haveMore {
//            return self.viewFooter
//        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
}
