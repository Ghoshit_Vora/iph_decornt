//
//  SellWithUSVC.swift
//  IPH_Decornt
//
//  Created by xx on 24/08/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class SellWithUSVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var tvProductDetails: UITextView!
    @IBOutlet weak var txtCompanyName: UITextField!
    @IBOutlet weak var txtGSTNNumber: UITextField!
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Method
    
    //---------------------------------------------
    
    func setupView() {
        
        
        self.setTitle(title: "Sell With Us")
        self.setLeftBar(isMenuRequired: true)
        
        
                if let name = UserDefaults.UserData.object(forKey: .name) as? String {
                    self.txtName.text = name
                }
        
                if let email = UserDefaults.UserData.object(forKey: .email) as? String {
                    self.txtEmail.text = email
                }
        
                if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
                    self.txtMobile.text = phone
                }
    }
    
    //---------------------------------------------
    
    //MARK:- Action Method
    
    //---------------------------------------------
    
    @IBAction func btnSubmitTapped(_ sender: Any) {
        
        let validator = Validator()
        validator.registerField(self.txtName, rule: [RequiredRule(message: "Please enter name")])
        validator.registerField(self.txtCompanyName, rule: [RequiredRule(message: "Please enter company name")])
        validator.registerField(self.txtEmail, rule: [RequiredRule(message: "Please enter email address"), EmailRule(rulePattern: .regular, message: "Please enter valid email address")])
        validator.registerField(self.txtMobile, rule: [RequiredRule(message: "Please enter mobile number"), PhoneNumberRule(message: "Please enter valid mobile number") ])
        
    }
    
    //---------------------------------------------
    
    //MARK:- WS Method
    
    //---------------------------------------------
    
    func callWSToSendSellToUs() {
        //https://www.decornt.com/mapp/index.php?view=sell&name=virag&cemail=virag@thedezine.in&cname=Thedezine&phone=9510069163&vat=147147&pan=Pfass&msg=test&custID=MTg=&phoneType=iphone
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        let apiName = "sell&name=\(self.txtName.text!)&email=\(self.txtEmail.text!)&contact_no=\(self.txtMobile.text!)&msg=\(self.tvProductDetails.text!)&vat=\(self.txtGSTNNumber.text ?? "0")&cname=\(self.txtCompanyName.text!)&pan=Pfass&custID=\(strCustId)&phoneType=iphone"
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            if success {
                var helpData:[String : Any] = response as! [String : Any]
                
                self.showAlert(App.AppName, message: helpData["message"] as? String)
                
                UserDefaults.UserData.set(self.txtName.text as AnyObject, forKey: UserDefaults.UserData.ObjectDefaultKey.name)
                
                
                
                UserDefaults.UserData.set(self.txtEmail.text as AnyObject, forKey: UserDefaults.UserData.ObjectDefaultKey.email)
                
                UserDefaults.UserData.set(self.txtMobile.text as AnyObject, forKey: UserDefaults.UserData.ObjectDefaultKey.phone)
                
            } else {
                self.showAlert(App.AppName, message: response as? String)
            }
        }
        
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}
