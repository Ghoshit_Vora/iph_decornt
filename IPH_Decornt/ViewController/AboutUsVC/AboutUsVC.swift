//
//  AboutUsVC.swift
//  IPH_Avail_Customer
//
//  Created by xx on 21/05/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit


class AboutUsVC: BaseVC {

    //MARK:-Properties
    
    @IBOutlet weak var imgAboutUs: UIImageView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var const_webview_Height: NSLayoutConstraint!
    var arrLinks:[[String:String]] = []
    
    
    //MARK:-Memory Management Methods
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Custom Method
    
    func setupView() {
        
        self.setTitle(title: "About Decornt")
        
        self.setLeftBar(isMenuRequired: true)
     
        self.webView.delegate = self
        setupData()
    }
    
    func setupData() {
        self.callAboutUsWS()
    }
    
    //MARK:-WS Method
    
    func callAboutUsWS() {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        //https://www.decornt.com/mapp/index.php?view=about&custID=MTg=&custPhone=9510069163&phoneType=iphone
        let apiName = "about&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                var aboutData:[String : Any] = response as! [String : Any]
                
                
                if let aboutTmpData = aboutData["about"] as? [[String : Any]] {
                    
                    let abData = aboutTmpData[0]
                    let strAboutData = abData["text"] as! String
                    
                    self.arrLinks = abData["links"] as! [[String : String]]
                    self.webView.loadHTMLString(strAboutData, baseURL: nil)
                }
                
                
                }
                
            }
        }
    
    //MARK:-Action Method
    
    @IBAction func btnVisitTapped(_ sender: Any) {
        
        if arrLinks.count > 0 {
            
            let strLinkData = self.arrLinks[0]
            guard let url = URL(string: strLinkData["link"]!) else {
                return //be safe
            }

            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    
    @IBAction func btnTearmTapped(_ sender: Any) {
        if arrLinks.count > 0 {
            
            let strLinkData = self.arrLinks[1]
            guard let url = URL(string: strLinkData["link"]!) else {
                return //be safe
            }

            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func btnprivatePolicyTapped(_ sender: Any) {
        if arrLinks.count > 0 {
            
            let strLinkData = self.arrLinks[2]
            guard let url = URL(string: strLinkData["link"]!) else {
                return //be safe
            }

            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    
    @IBAction func btnDisClaimerTapped(_ sender: Any) {
        if arrLinks.count > 0 {
            
            let strLinkData = self.arrLinks[3]
            guard let url = URL(string: strLinkData["link"]!) else {
                return //be safe
            }

            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    //MARK:-View Life Cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }

}

extension AboutUsVC:UIWebViewDelegate {
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webView.frame.size = webView.sizeThatFits(.zero)
        webView.scrollView.isScrollEnabled=false;
        const_webview_Height.constant = webView.scrollView.contentSize.height
        webView.scalesPageToFit = true
    }
}
