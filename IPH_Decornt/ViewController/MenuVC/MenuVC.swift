//
//  MenuVC.swift
//  IPH_Decornt
//
//  Created by xx on 21/08/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit
import SJSwiftSideMenuController

class MenuVC: UIViewController {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var viewMenuTop: UIView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var menuItems : NSArray = NSArray()
    var menuImages: NSArray = NSArray()
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Method
    
    //---------------------------------------------
    
    func setUpView() {
        
        menuItems = [
            "Home",
            "Categories",
            "Notification",
            "My Account",
            "My Wallet",
            "My Cart",
            "My WishList",
            "My Order",
            "Refer Your Friend",
            "About Decorant",
            "Contact Us",
            "Help/Feedback",
            "Rate Us",
            "Sell With Us",
            "Logout"
        ]
        
        menuImages = [
            "imgHome",
            "imgCategories",
            "imgNotification",
            "imgMyAccount",
            "imgMyWallet",
            "imgSellWithUs",
            "imgMyWishList",
            "imgMyOrder",
            "imgReferFriends",
            "imgAbout",
            "imgContactUs",
            "imgContactUs",
            "imgRate",
            "imgSellWithUs",
            "imgLogOut"
        ]
        
        
        self.tblMenu.delegate = self
        self.tblMenu.dataSource = self
        self.tblMenu.reloadData()
        
//        self.imgProfileView.layer.cornerRadius = imgProfileView.frame.height / 2
//        self.imgProfileView.layer.borderColor = UIColor.white.cgColor
//        self.imgProfileView.layer.borderWidth = 1.0
//        self.lblName.text = "Peter Parker"
//        self.lblPhoneNo.text = "+9198234678879"
        
        
        if let name = UserDefaults.UserData.object(forKey: .name) as? String {
            self.lblName.text = name
        }
        
        if let mobile = UserDefaults.UserData.object(forKey: .phone) as? String {
            self.lblMobile.text = mobile
        }
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Method
    
    //---------------------------------------------
    
    func tableViewCellSelected(at indexPath: IndexPath) {
        
        switch indexPath.row {
            
        // Home
        case 0 :
            self.setHomeView()
            break

        case 1 :
            self.setCategoryView()
            break
            
        case 2 :
            self.setNotificaionView()
            break
            
        case 3 :
            //self.setContactView()
            break
            
        // Notification
        case 4 :
            self.setAccountView()
            break
            
        case 5 :
            self.setWalletView()
            break
            
        // Refer Friend
        case 6 :
            setMyCartView()
            break
            
        // About Us
        case 7 :
            
            break
            
        // Help
        case 8 :
            self.setOrderView()
            break
            
        // Logout
        case 9 :
            self.setReferView()
            //self.loginConfirmation()
            break
            
        case 11:
            setAboutView()
            break
            
        case 12 :
            setContactView()
            break
            
        case 13 :
            self.setHelpView()
            break
            
        case 14:
            //Rate
            break
        case 15 :
            self.setSellWithUsView()
            break
            
        case 17 :
            self.loginConfirmation()
            break
            
        // Other Cell
        default :
            print("Other Cell Selected")
            break
        }
    }
    
    
    func setHomeView()  {
        let homeVC = AppStoryboard.Dashboard.viewController(viewControllerClass: DashboardVC.self)
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: homeVC)
    }
    
    func setCategoryView()  {
        let categoryVC = AppStoryboard.Product.viewController(viewControllerClass: CategoryVC.self)
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: categoryVC)
    }
    
    func setNotificaionView() {
        let NotificaionVC = AppStoryboard.Notification.viewController(viewControllerClass: NotificationVC.self)
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: NotificaionVC)
    }
    
    func setAccountView() {
        let accountVC = AppStoryboard.Authotication.viewController(viewControllerClass: MyAccountVC.self)
        
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: accountVC)
    }
    
    func setOrderView() {
        let orderVC = AppStoryboard.Product.viewController(viewControllerClass: MyOrderVC.self)
        
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: orderVC)
    }
    
    func setWalletView() {
        let walletVC = AppStoryboard.MyWallet.viewController(viewControllerClass: MyWalletVC.self)
        
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: walletVC)
    }
    
    func setReferView() {
        let referVC = AppStoryboard.ContactUs.viewController(viewControllerClass: RefereVC.self)
        
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: referVC)
    }
    
    func setContactView() {
        let contactVC = AppStoryboard.ContactUs.viewController(viewControllerClass: ContactUsVC.self)
        
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: contactVC)
    }
    
    
    func setAboutView() {
        let aboutVC = AppStoryboard.ContactUs.viewController(viewControllerClass: AboutUsVC.self)
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: aboutVC)
    }
    
    func setHelpView() {
        let helpVC = AppStoryboard.ContactUs.viewController(viewControllerClass: HelpVC.self)
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: helpVC)
    }
    
    func setSellWithUsView() {
        let sellWithUsVC = AppStoryboard.ContactUs.viewController(viewControllerClass: SellWithUSVC.self)
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: sellWithUsVC)
    }
    
    func setMyCartView() {
        let myCartVC = AppStoryboard.MyCart.viewController(viewControllerClass: MyCartVC.self)
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: myCartVC)
    }
    
    func loginConfirmation() {
        
        SJSwiftSideMenuController.hideLeftMenu()
        
        let actionOK = UIAlertAction(title: "Ok", style: .default) { (action) in
            let homeVC = AppStoryboard.Authotication.viewController(viewControllerClass: LoginVC.self)
            let navigationController = UINavigationController(rootViewController: homeVC)
            AppDelegate.sharedInstance.window?.rootViewController = navigationController
            UserDefaults.Account.set(false, forKey: UserDefaults.Account.BoolDefaultKey.isUserLoggedIn)
        }
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        self.showAlert(App.AppName, message: "Do you want to logout?", alertActions: [actionOK, actionCancel])
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}

extension MenuVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count + 3
    }
    
    //---------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // This cell will seperator
        if indexPath.row == 3 || indexPath.row == 10 || indexPath.row == 16 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SeperatorCell", for: indexPath)
            cell.isUserInteractionEnabled = false
            return cell
        }
        
        let cell:MenuListCell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuListCell
        
        if indexPath.row >= 0 && indexPath.row <= 2 {
            cell.lblTitle.text = menuItems[indexPath.row] as? String
            cell.imgView.image = UIImage(named: menuImages[indexPath.row] as! String)
        } else if indexPath.row >= 4 && indexPath.row <= 9 {
            cell.lblTitle.text = menuItems[indexPath.row-1] as? String
            cell.imgView.image = UIImage(named: menuImages[indexPath.row-1] as! String)
        } else if indexPath.row >= 11 && indexPath.row <= 15 {
             cell.lblTitle.text = menuItems[indexPath.row-2] as? String
            cell.imgView.image = UIImage(named: menuImages[indexPath.row-2] as! String)
        } else {
            cell.lblTitle.text = menuItems[indexPath.row-3] as? String
            cell.imgView.image = UIImage(named: menuImages[indexPath.row-3] as! String)
        }
        
        return cell
    }
    
    //---------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableViewCellSelected(at: indexPath)
    }
    
    //---------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3 || indexPath.row == 10 || indexPath.row == 16 {
            return 15
        }

        return 40
    }
}

