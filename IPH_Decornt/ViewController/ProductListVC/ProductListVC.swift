//
//  ProductListVC.swift
//  IPH_Decornt
//
//  Created by xx on 26/08/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

enum LoadMoreStatus{
    case loading
    case finished
    case haveMore
}

class ProductListVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var tblProduct: UITableView!
    @IBOutlet weak var collectionProduct: UICollectionView!
    
    @IBOutlet weak var btnListSelection: UIButton!
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var totalNumberOfProduct : String = "0"
    var ProductListResul = NSMutableArray()
    var categoryResultData = NSDictionary(){
        didSet{
            self.GetProductResult(sortType: SortingEnableValue, pageCode: currentPageCode)
        }
    }
    var isListViewEnable : Bool = false
    var SortingEnableValue : String = ""
    var currentPageCode : Int = 0
    
    var loadingStatus = LoadMoreStatus.haveMore
    
    var PAGING_LIMIT : Int = 20
    var isListSelect = Bool()
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Method
    
    //---------------------------------------------
    
    func setupView() {
        
        if let tempStr = categoryResultData.object(forKey: "name") as? String {
            self.setTitle(title: tempStr)
        }
        
        self.setLeftBar(isMenuRequired: false)
        
        
        let nibName = UINib(nibName: "ProductListCell", bundle: nil)
        self.tblProduct .register(nibName, forCellReuseIdentifier: "ProductListCell")
        
        let nibName5 = UINib(nibName: "ProducGridCell", bundle:nil)
        self.collectionProduct.register(nibName5, forCellWithReuseIdentifier: "ProducGridCell")
        
        self.collectionProduct.isHidden = true
        self.tblProduct.isHidden = false
        
        self.setRightData(isSearchBtnRequired: true)
        
        self.isListSelect = true
    }
    
    //---------------------------------------------
    
    @objc func loadMore() {
        //if loadingStatus != .finished {
            self.currentPageCode = self.currentPageCode + 1
            
            self.GetProductResult(sortType: self.SortingEnableValue, pageCode: self.currentPageCode)
            
//        }else{
//
//        }
        
    }
    

    @IBAction func btnListSelectionTapped(_ sender: Any) {
        
        if isListSelect {
            self.isListSelect = false
            self.tblProduct.isHidden = true
            self.collectionProduct.isHidden = false
            self.btnListSelection.setImage(#imageLiteral(resourceName: "imgProductList"), for: .normal)
        } else {
            self.isListSelect = true
            self.tblProduct.isHidden = false
            self.collectionProduct.isHidden = true
            self.btnListSelection.setImage(#imageLiteral(resourceName: "imgGridProductList"), for: .normal)
        }
    }
    
    //---------------------------------------------
    
    //MARK:- WS Methods
    
    //---------------------------------------------
    
    func GetProductResult(sortType : String, pageCode : Int){
        
//        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
//            
//            return
//        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        var CategoryId = ""
        if let tempStr = categoryResultData.object(forKey: "catID") as? String {
            CategoryId = tempStr
        }
        
        self.SortingEnableValue = sortType
        
        var requestUrl = ""
        
        //https://www.decornt.com/mapp/index.php?view=product&catID=Njg=&pagecode=0&custID=MTg=&custPhone=9510069163&phoneType=iphone
        if sortType.isEmpty == true {
            requestUrl = String(format: "product&catID=%@=&pagecode=%d&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone", arguments: [CategoryId,pageCode])
        }else{
           //https://www.decornt.com/mapp/index.php?view=product&catID=Njg=&pagecode=0&type=name_z_a&custID=MTg=&custPhone=9510069163&phoneType=iphone
            
            
            
            requestUrl = String(format: "product&catID=%@=&pagecode=%d&custID=MTg=&custPhone=9510069163&phoneType=iphone&type=%@", arguments: [CategoryId,pageCode,sortType])
        }
        
        print(requestUrl)
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: requestUrl) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                var productData:[String : Any] = response as! [String : Any]
                
                
                if let tempStr = productData["product_count"] as? String {
                    self.totalNumberOfProduct = tempStr
                }
                if let tempStr = productData["product_count"] as? NSNumber {
                    self.totalNumberOfProduct = tempStr.stringValue
                }
                
                
                if let resultList = productData["product_list"] as? NSArray {
                    //                        self.ProductListResul = NSMutableArray(array: resultList)
                    
                    for tempData in resultList {
                        self.ProductListResul.add(tempData)
                    }
                    DispatchQueue.main.async
                        {
                            if pageCode <= 0 {
                                self.tblProduct.delegate = self
                                self.tblProduct.dataSource = self
                                self.tblProduct.reloadData()
                            }else{
                                self.tblProduct.reloadData()
                            }
                    }
                    DispatchQueue.main.async
                        {
                            if pageCode <= 0 {
                                self.collectionProduct.delegate = self
                                self.collectionProduct.dataSource = self
                            }
                            self.collectionProduct.reloadData()
                            
                    }
                    
                    
                    self.loadingStatus = .finished
                    
                } else {
                    
                }
                
            } else {
                self.showAlert(App.AppName, message: response as? String)
                
                self.loadingStatus = .finished
            }
            
            DispatchQueue.main.async {
                self.tblProduct.tableFooterView = UIView()
            }
        }
        
    }
    
    @IBAction func btnSortByTapped(_ sender: Any) {
        
        self.currentPageCode = 0
        
        let optionMenu = UIAlertController(title: nil, message: "SORT BY", preferredStyle: .actionSheet)
        
        let nameAtoZ = UIAlertAction(title: "Name A To Z", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if self.ProductListResul.count > 0 {
                self.ProductListResul.removeAllObjects()
            }
            self.GetProductResult(sortType: "name_a_z", pageCode: self.currentPageCode)
            
        })
        let nameZtoA = UIAlertAction(title: "Name Z To A", style: .default, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
            if self.ProductListResul.count > 0 {
                self.ProductListResul.removeAllObjects()
            }
                self.GetProductResult(sortType: "name_z_a", pageCode: self.currentPageCode)
            
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler:
        {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        optionMenu.addAction(nameAtoZ)
        optionMenu.addAction(nameZtoA)
        optionMenu.addAction(cancelAction)
        
        DispatchQueue.main.async
            {
                self.present(optionMenu, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func btnResetTapped(_ sender: Any) {
        
        if self.ProductListResul.count > 0 {
            self.ProductListResul.removeAllObjects()
        }
        
        self.currentPageCode = 0
        
        self.GetProductResult(sortType: "", pageCode: currentPageCode)
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}

extension ProductListVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ProductListResul.count
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 117
//    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        if loadingStatus == .haveMore {
//            return self.footerView
//        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if loadingStatus == .haveMore {
            return 60.0
        }
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    //MARK:- Show Number Of Record Toast-
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if self.isListViewEnable == true {
            let firstVisibleIndexPath = self.tblProduct.indexPathsForVisibleRows?[0]
            if firstVisibleIndexPath != nil {
                if let rows = firstVisibleIndexPath?.row {
                    let string = String(format: "Showing %d/%@ items", arguments: [rows + 1,self.totalNumberOfProduct])
                    //                    self.view.makeToast(string, duration: 1.0, position: CSToastPositionTop)
                }
                
            }
        }else{
            
            let firstVisibleIndexPath = self.collectionProduct.indexPathsForVisibleItems[0]
            let string = String(format: "Showing %d/%@ items", arguments: [firstVisibleIndexPath.row + 1,self.totalNumberOfProduct])
            //            self.view.makeToast(string, duration: 1.0, position: CSToastPositionTop)
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int){
        if loadingStatus == .haveMore {
            loadingStatus = .loading
            self.perform(#selector(loadMore), with: nil, afterDelay: 0.5)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.ProductListResul.count > indexPath.row {
            if let data = self.ProductListResul.object(at: indexPath.row) as? NSDictionary {
                if let cell = self.tblProduct.dequeueReusableCell(withIdentifier: "ProductListCell") as? ProductListCell {
                    cell.resultData = data
                    
                    if self.ProductListResul.count >= PAGING_LIMIT {
                        if indexPath.row == (self.ProductListResul.count - 1) {
                            
                                self.loadMore()
                            
                        }
                    }
                    
                    return cell
                }
            }
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let cell = tableView.cellForRow(at: indexPath) as? ProductListCell {
            let objRoot = AppStoryboard.Product.viewController(viewControllerClass: ProductDetailsVC.self)
            objRoot.productResult = cell.resultData
           self.navigationController?.pushViewController(objRoot, animated: true)
        }
    }

}

extension ProductListVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if loadingStatus == .haveMore {
            return (loadingStatus == .finished) ? CGSize.zero : CGSize(width: self.view.frame.width, height: 60)
        }else{
            return CGSize.zero
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        if loadingStatus == .haveMore {
            loadingStatus = .loading
            //self.perform(#selector(ProductViewController.loadMore), with: nil, afterDelay: 0.5)
        }
        
        
    }

    //MARK: - UICollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //        return CGSize(width: 100, height: 100);
        return CGSize(width: (UIScreen.main.bounds.width - 1) / 2, height: 275);
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.ProductListResul.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.ProductListResul.count > indexPath.row {
            if let data = self.ProductListResul.object(at: indexPath.row) as? NSDictionary {
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProducGridCell", for: indexPath) as? ProducGridCell {
                    
                    cell.resultData = data
                    return cell
                }
            }
            
            
        }
        
        
        return UICollectionViewCell()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if let cell = collectionView.cellForItem(at: indexPath) as? ProducGridCell {
            let objRoot = AppStoryboard.Product.viewController(viewControllerClass: ProductDetailsVC.self)
            objRoot.productResult = cell.resultData
            self.navigationController?.pushViewController(objRoot, animated: true)
        }

    }
}
