//
//  ProductListCell.swift
//  Sabzilana
//
//  Created by TNM3 on 3/30/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class ProductListCell: UITableViewCell {

    
    @IBOutlet var imageview : UIImageView!
    @IBOutlet var nameLabel : UILabel!
    @IBOutlet var priceLabel : UILabel!
    @IBOutlet var mrpLabel : UILabel!
    @IBOutlet var btnFavourite : UIButton!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblMrp: UILabel!
    @IBOutlet weak var lblStock: UILabel!
    @IBOutlet weak var lblQtyMsg: UILabel!
    
    var resultData = NSDictionary() {
        didSet{
            self.SetResultUI()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
   
  
    
    @objc func SetResultUI(){
        
        if self.imageview != nil && self.resultData.object(forKey: "productID") != nil {
            
        
            if let tempStr = resultData.object(forKey: "image") as? String {
                if tempStr.isEmpty == false {
                    let imageUrl = URL(string: tempStr)
                    self.imageview.sd_setImage(with: imageUrl, placeholderImage: UIImage.init())
                    
                }
            }
            
            if let tempStr = resultData.object(forKey: "name") as? String {
                self.nameLabel.text = tempStr
            }
            
            if let tempStr = resultData.object(forKey: "price") as? String {
                let priceStr = String(format: "₹ %@", arguments: [tempStr])
                self.lblPrice.isHidden = false
                self.lblPrice.text = priceStr
                
            }
            
            if let tempStr = resultData.object(forKey: "mrp") as? String {
                if tempStr.isEmpty == false {
                    self.lblMrp.isHidden = false
                    self.lblMrp.text = String(format: "₹ %@", arguments: [tempStr])
                    let attributedString = NSMutableAttributedString(string: self.mrpLabel.text!)
                    attributedString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 1, range: NSMakeRange(0, attributedString.length))
                    self.mrpLabel.attributedText = attributedString
                }else{
                    self.lblMrp.isHidden = true
                }
                
                
            }
            
            if let tempStrDiscount = resultData.object(forKey: "discount") as? String {
                
                self.lblDiscount.isHidden = false
                self.lblDiscount.text = tempStrDiscount
                
            }
            
            if let tempStrQtyMsg = resultData.object(forKey: "qty_message") as? String {
                
                self.lblQtyMsg.isHidden = false
                self.lblQtyMsg.text = tempStrQtyMsg
                
            }
            
            
            //Qty Sold Out
            if let tempStr = resultData.object(forKey: "sold_out") as? String {
                if tempStr.caseInsensitiveCompare("yes") == .orderedSame {
//                    self.buttonAddQty.isHidden = true
//                    self.qtyView.isHidden = true
                }
            }
            
        }else{
            self.perform(#selector(ProductListCell.SetResultUI))
        }
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
