//
//  ProducGridCell.swift
//  Sabzilana
//
//  Created by TNM3 on 3/30/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class ProducGridCell: UICollectionViewCell {

    @IBOutlet var btnFavourite : UIButton!

    @IBOutlet var imageview : UIImageView!
    @IBOutlet var nameLabel : UILabel!

    @IBOutlet var discountLabel : UILabel!
    @IBOutlet var priceLabel : UILabel!
    @IBOutlet var mrpLabel : UILabel!

    
    var resultData = NSDictionary() {
        didSet{
            self.SetResultUI()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    

    @objc func SetResultUI(){
        
        if self.imageview != nil && self.resultData.object(forKey: "productID") != nil {
            

            if let tempStr = resultData.object(forKey: "image") as? String {
                if tempStr.isEmpty == false {
                    let imageUrl = URL(string: tempStr)
                    self.imageview.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                        if image != nil {
                            self.imageview.image = image
                        }
                    })
                    
                }
            }
            
            if let tempStr = resultData.object(forKey: "name") as? String {
                self.nameLabel.text = tempStr
            }
            
            //Get Prices Data
            if let priceList = resultData.object(forKey: "price_list") as? NSArray {
                if priceList.count > 0 {
                    if let priceData = priceList.object(at: 0) as? NSDictionary {
                        
                    }
                }
                
            }
            if let tempStr = resultData.object(forKey: "price") as? String {
                
                self.priceLabel.text = String(format: "₹ %@", arguments: [tempStr])
                
            }
            if let tempStr = resultData.object(forKey: "mrp") as? String {
                if tempStr.isEmpty == false {
                    self.mrpLabel.isHidden = false
                    self.mrpLabel.text = String(format: "₹ %@", arguments: [tempStr])
                    let attributedString = NSMutableAttributedString(string: self.mrpLabel.text!)
                    attributedString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 1, range: NSMakeRange(0, attributedString.length))
                    self.mrpLabel.attributedText = attributedString
                }else{
                    self.mrpLabel.isHidden = true
                }
                
                
            }
            
            
            if let tempStr = resultData.object(forKey: "discount") as? String {
                if tempStr.isEmpty == false || tempStr == "0" {
                    self.discountLabel.isHidden = false
                    self.discountLabel.text = String(format: "%@", arguments: [tempStr])
                }else{
                    self.discountLabel.isHidden = true
                }
                
            }
            
            
            
            
            //Qty Sold Out
            if let tempStr = resultData.object(forKey: "sold_out") as? String {
                if tempStr.caseInsensitiveCompare("yes") == .orderedSame {
//                    self.buttonAddQty.isHidden = true
//                    self.qtyView.isHidden = true
                }
            }
        
        }else{
            self.perform(#selector(ProducGridCell.SetResultUI))
        }
        
    }
}
