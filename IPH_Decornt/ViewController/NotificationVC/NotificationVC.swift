//
//  NotificationVC.swift
//  IPH_Decornt
//
//  Created by xx on 24/08/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit
import SDWebImage

class NotificationVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var tblNotificationList: UITableView!
    
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var arrNotificationData:[[String:Any]] = []
    var isFromMenu : Bool = true
    var defaultName: String = "Notification"
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Method
    
    //---------------------------------------------
    
    func setupView() {
        
        
        self.setTitle(title: defaultName)
        
//        if isFromMenu {
//            self.setLeftBar(isMenuRequired: true)
//        }
        self.setLeftBar(isMenuRequired: true)
        self.callGetNotifications()
    }
    
    //---------------------------------------------
    
    //MARK:- Call WS Method
    
    //---------------------------------------------
    
    func callGetNotifications() {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        //https://www.decornt.com/mapp/index.php?view=offer_list&pagecode=0&custID=MTg=&custPhone=9510069163&phoneType=iphone
        let apiName = "offer_list&pagecode=0&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                var notificationData:[String : Any] = response as! [String : Any]
                self.arrNotificationData = notificationData["offer_list"] as! [[String : Any]]
                
                if self.arrNotificationData.count > 0 {
                    //self.lblNoDataFound.isHidden = true
                    self.tblNotificationList.reloadData()
                    self.tblNotificationList.isHidden = false
                } else {
                    self.tblNotificationList.isHidden = true
                    //self.lblNoDataFound.isHidden = false
                }
                
            } else {
                self.showAlert(App.AppName, message: response as? String)
            }
        }
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}

extension NotificationVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrNotificationData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomNotificationCell") as! CustomNotificationCell
        
        if arrNotificationData.count > 0 {
            let data = self.arrNotificationData[indexPath.row]
            
            cell.lblNotificationName.text = data["name"] as? String
            
            let strUrl = URL(string: data["image"] as! String)
            cell.imgNotification.sd_setImage(with: strUrl, placeholderImage: UIImage.init())
        }
        
        return cell
    }
}

