//
//  RefereVC.swift
//  IPH_Avail_Customer
//
//  Created by Sagar Chauhan on 03/07/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class RefereVC: BaseVC {

    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Outlets
    //---------------------------------------------------------------------------
    
    @IBOutlet weak var labelReferYourFriend                 : UILabel!
    @IBOutlet weak var ivReferFriend                        : UIImageView!
    @IBOutlet weak var labelShareVia                        : UILabel!
    @IBOutlet weak var btnWhatsAppShare                     : UIButton!
    @IBOutlet weak var btnFBShare                           : UIButton!
    @IBOutlet weak var btnGmailShare                        : UIButton!
    @IBOutlet weak var btnMore                              : UIButton!
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Variables
    //---------------------------------------------------------------------------
    
    var shareMsg = String()
    var shareImg = String()
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Memory Management Methods
    //---------------------------------------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Action Methods
    //---------------------------------------------------------------------------
    
    @IBAction func btnWhatsAppShareTapped(_ sender: UIButton) {
        let strMessage = self.shareMsg
        
        let activityVC = UIActivityViewController(activityItems: [strMessage], applicationActivities: nil)
        
        if #available(iOS 11.0, *) {
            activityVC.excludedActivityTypes = [
                .airDrop,
                .assignToContact,
                .print,
                .saveToCameraRoll,
                .openInIBooks,
                .markupAsPDF
            ]
        } else {
            activityVC.excludedActivityTypes = [
                .airDrop,
                .assignToContact,
                .print,
                .saveToCameraRoll,
                .openInIBooks,
            ]
        }
        
        self.present(activityVC, animated: true, completion: nil)
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnFBShareTapped(_ sender: UIButton) {
        let strMessage = self.shareMsg
        
        let activityVC = UIActivityViewController(activityItems: [strMessage], applicationActivities: nil)
        
        if #available(iOS 11.0, *) {
            activityVC.excludedActivityTypes = [
                .airDrop,
                .assignToContact,
                .print,
                .saveToCameraRoll,
                .openInIBooks,
                .markupAsPDF
            ]
        } else {
            activityVC.excludedActivityTypes = [
                .airDrop,
                .assignToContact,
                .print,
                .saveToCameraRoll,
                .openInIBooks,
            ]
        }
        
        self.present(activityVC, animated: true, completion: nil)
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnGmailShareTapped(_ sender: UIButton) {
        let strMessage = self.shareMsg
        
        let activityVC = UIActivityViewController(activityItems: [strMessage], applicationActivities: nil)
        
        if #available(iOS 11.0, *) {
            activityVC.excludedActivityTypes = [
                .airDrop,
                .assignToContact,
                .print,
                .saveToCameraRoll,
                .openInIBooks,
                .markupAsPDF
            ]
        } else {
            activityVC.excludedActivityTypes = [
                .airDrop,
                .assignToContact,
                .print,
                .saveToCameraRoll,
                .openInIBooks,
            ]
        }
        
        self.present(activityVC, animated: true, completion: nil)
    }
    
    //---------------------------------------------------------------------------
    
    @IBAction func btnMoreTapped(_ sender: UIButton) {
        
        let strMessage = self.shareMsg
        
        let activityVC = UIActivityViewController(activityItems: [strMessage], applicationActivities: nil)
        
        if #available(iOS 11.0, *) {
            activityVC.excludedActivityTypes = [
                .airDrop,
                .assignToContact,
                .print,
                .saveToCameraRoll,
                .openInIBooks,
                .markupAsPDF
            ]
        } else {
            activityVC.excludedActivityTypes = [
                .airDrop,
                .assignToContact,
                .print,
                .saveToCameraRoll,
                .openInIBooks,
            ]
        }
        
        self.present(activityVC, animated: true, completion: nil)
    }
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Custom Methods
    //---------------------------------------------------------------------------
    
    func setupView() {
       self.setTitle(title: "Refer your friend")
        self.setLeftBar(isMenuRequired: true)
        self.callWSToGetReferData()
    }
    
    //---------------------------------------------------------------------------
    
    
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- Web Service Call Methods
    //---------------------------------------------------------------------------
    
    func callWSToGetReferData() {
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        //https://www.decornt.com/mapp/index.php?view=share_msg&custID=MTg=&custPhone=9510069163&phoneType=iphone
        let apiName = "share_msg&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                let shareData:[String : Any] = response as! [String : Any]
                
                let arrImgData = shareData["share_data"] as! [[String : Any]]
                
                let dictData = arrImgData[0] 
                
                if let imgShare = dictData["image"] as? String {
                    if let imgUrl = URL(string: imgShare) {
                        self.ivReferFriend.sd_setImage(with: imgUrl, completed: nil)
                    }
                }
                
                if let strMsg = dictData["message"] as? String {
                    self.shareMsg = strMsg
                }

            }
        }
    }
    
    //---------------------------------------------------------------------------
    // MARK:-
    // MARK:- View Life Cycle Methods
    //---------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
    }
}

