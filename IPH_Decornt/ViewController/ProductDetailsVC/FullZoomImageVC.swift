//
//  FullZoomImageVC.swift
//  IPH_Decornt
//
//  Created by xx on 23/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class FullZoomImageVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var imgView: UIImageView!
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var imageList : NSArray = NSArray()
    var productResult = NSDictionary()
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Methods
    
    //---------------------------------------------
    
    func setUpView() {
        
        if self.imageList.count > 0 {
            if let resultData = imageList.object(at: 0) as? NSDictionary {
                if let tempStr = resultData.object(forKey: "large_image") as? String {
                    if tempStr.isEmpty == false {
                        //let imageUrl = URL(string: tempStr.EncodeUrlFormat(text: tempStr))
                        
                        let url = URL(string: tempStr)
                        let data = try? Data(contentsOf: url!)
                        self.imgView.image = UIImage(data: data!)
                        
                    }
                }
            }
        }
        
        
        let nibName5 = UINib(nibName: "ImageViewCell", bundle:nil)
        self.collectionView.register(nibName5, forCellWithReuseIdentifier: "ImageViewCell")
        
        
        
            if let tempStr = self.productResult.object(forKey: "name") as? String {
                if tempStr.isEmpty == false {
                    self.setTitle(title: tempStr)
                }
                
        }
        
        self.setLeftBar(isMenuRequired: false)
       
        self.LoadResult()
    }
    
    func LoadResult(){
        if self.collectionView != nil {
            DispatchQueue.main.async
                {
                    self.collectionView.delegate = self
                    self.collectionView.dataSource = self
                    self.collectionView.reloadData()
                    
            }
        }else{
            self.perform(#selector(HeaderHomeCell.LoadResult), with: nil, afterDelay: 0.2)
        }
        
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}

extension FullZoomImageVC : UICollectionViewDelegate, UICollectionViewDataSource  {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        //        return CGSize(width: 100, height: 100);
        return CGSize(width: 70, height: 70);
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.imageList.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if self.imageList.count > indexPath.row {
            if let result = self.imageList.object(at: indexPath.row) as? NSDictionary {
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageViewCell", for: indexPath) as? ImageViewCell {
                    cell.resultData = result
                    
                    if let tempStr = result.object(forKey: "large_image") as? String {
                        if tempStr.isEmpty == false {
                            let imageUrl = URL(string:tempStr)
                            cell.imageview.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                                if image != nil {
                                    
                                    cell.imageview.image = image
                                }
                            })
                            
                        }
                    }
                    
                    return cell
                }
            }
        }
        
        return UICollectionViewCell()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if let cell = collectionView.cellForItem(at: indexPath) as? ImageViewCell {
            if let tempStr = cell.resultData.object(forKey: "large_image") as? String {
                if tempStr.isEmpty == false {
                    
                    let url = URL(string: tempStr)
                    let data = try? Data(contentsOf: url!)
                    self.imgView.image = UIImage(data: data!)
                    
                }
            }
        }
        
    }
}
