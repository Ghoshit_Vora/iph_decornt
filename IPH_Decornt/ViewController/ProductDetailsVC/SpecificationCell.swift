//
//  SpecificationCell.swift
//  Sabzilana
//
//  Created by Desap Team on 27/01/2018.
//  Copyright © 2018 Sabzilana. All rights reserved.
//

import UIKit
protocol SpecificationCellDelegate {
    func writeReview(cell : SpecificationCell)
    func viewReviews(cell : SpecificationCell)
    func reloadWeb()
    func changePinCode()
}
class SpecificationCell: UITableViewCell, UIWebViewDelegate {

    var delegate : SpecificationCellDelegate?
    
    
    @IBOutlet var specificLabel : UILabel?
    @IBOutlet var reviewLabel : UILabel?
    @IBOutlet var specificLine : UIView?
    @IBOutlet var reviewLine : UIView?
    @IBOutlet var menuView : UIView?
    
    @IBOutlet var reviewContainerView : UIView?
    
    @IBOutlet weak var webSpecification: UIWebView!
    
    @IBOutlet weak var const_webView_Height: NSLayoutConstraint!
    
    @IBOutlet weak var lblPinCode: UILabel!
    var rateEZ = EZRatingView()
    
    var resultData = NSDictionary()
    var specificationDetailList = NSArray() {
        didSet{
            self.UpdateUI()
        }
    }
    
    @IBAction func submitReview(){
        if self.delegate != nil {
            self.delegate?.writeReview(cell: self)
        }
    }
    @IBAction func viewAllReview(){
        if self.delegate != nil {
            self.delegate?.viewReviews(cell: self)
        }
    }
    
    
    @IBAction func btnPinCodeTapped(_ sender: Any) {
        if self.delegate != nil {
            self.delegate?.changePinCode()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //let nibName = UINib(nibName: "SpecificationDetailsCell", bundle: nil)
        
    
//        self.UpdateUI()
        self.shadowView()
        self.setMenuSelect(index: 0)
        
        self.webSpecification.delegate = self
    }
    
 
    func shadowView(){
        self.reviewContainerView?.layer.masksToBounds = false
        self.reviewContainerView?.layer.shadowColor = UIColor.darkGray.cgColor
        self.reviewContainerView?.layer.shadowOpacity = 0.5
        self.reviewContainerView?.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.reviewContainerView?.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        
        self.layer.rasterizationScale = UIScreen.main.scale
        
        
       
    }
    func setMenuSelect(index : Int){
        if index == 0 {
            self.specificLabel?.textColor = UIColor.black
            self.specificLine?.backgroundColor = UIColor(red: 185/255.0, green: 0/255.0, blue: 66/255.0, alpha: 1.0)
            self.reviewLabel?.textColor = UIColor.darkGray
            self.reviewLine?.backgroundColor = UIColor.gray
            self.reviewContainerView?.isHidden = true
            self.webSpecification.isHidden = false
            //self.tableview?.isHidden = false
        }else{
            self.specificLabel?.textColor = UIColor.darkGray
            self.specificLine?.backgroundColor = UIColor.gray
            self.reviewLabel?.textColor = UIColor.black
            self.reviewLine?.backgroundColor = UIColor(red: 185/255.0, green: 0/255.0, blue: 66/255.0, alpha: 1.0)
            self.reviewContainerView?.isHidden = false
            //self.tableview?.isHidden = true
            self.webSpecification.isHidden = true
        }
    }
    @IBAction func specificationClick(){
        self.setMenuSelect(index: 0)
    }
    @IBAction func reviewClick(){
        self.setMenuSelect(index: 1)
    }
    @objc func UpdateUI(){
       
        if let tempStr = resultData.object(forKey: "description") as? String {
            //self.lblSpecification.text = tempStr
            self.webSpecification.loadHTMLString(tempStr, baseURL: nil)
        }
        
        if let tempStr = resultData.object(forKey: "pincode") as? String {
            self.lblPinCode.text = tempStr
        }
    }
    func UpdateTableView(){

    }

    @IBAction func btnWriteReviewTapped(_ sender: Any) {
        if self.delegate != nil {
            self.delegate?.writeReview(cell: self)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webView.frame.size = webView.sizeThatFits(.zero)
        webView.scrollView.isScrollEnabled=false;
        const_webView_Height.constant = webView.scrollView.contentSize.height
        webView.scalesPageToFit = true
        delegate?.reloadWeb()
    }
    
}

