//
//  PinCodeVC.swift
//  IPH_Decornt
//
//  Created by xx on 23/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class PinCodeVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var txtPincode: UITextField!
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Methods
    
    //---------------------------------------------
    
    func setUpview() {
        
        
    }
    
    //---------------------------------------------
    
    //MARK:- Action Methods
    
    //---------------------------------------------
    
    @IBAction func btnOkTapped(_ sender: Any) {
        if self.txtPincode.text == "" || self.txtPincode.text == String(describing: CharacterSet.whitespacesAndNewlines) {
            self.showAlertWithMessage("Please enter review")
        } else {
            self.callWSPinCode()
        }
    }
    
    //---------------------------------------------
    
    //MARK:- CallWS Methods
    
    //---------------------------------------------
    
    func callWSPinCode() {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        ///https://www.decornt.com/mapp/index.php?view=check_ship&post=370465&productID=ODA4&custID=MTg=&custPhone=9510069163&phoneType=iphone
        let apiName = "check_ship&post=\(self.txtPincode.text!)&productID=ODA4&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            if success {
               
               self.dismiss(animated: true, completion: nil)
                
            } else {
                self.showAlert(App.AppName, message: response as? String)
            }
        }
        
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}
