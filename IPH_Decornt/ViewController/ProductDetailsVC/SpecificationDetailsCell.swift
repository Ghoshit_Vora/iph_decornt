//
//  SpecificationDetailsCell.swift
//  Sabzilana
//
//  Created by Desap Team on 27/01/2018.
//  Copyright © 2018 Sabzilana. All rights reserved.
//

import UIKit

class SpecificationDetailsCell: UITableViewCell {

    @IBOutlet var attributeLabel : UILabel?
    @IBOutlet var valueLabel : UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
