//
//  ProductReviewVC.swift
//  IPH_Decornt
//
//  Created by xx on 07/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class ProductReviewVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var rateView: UIView!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var rateEZ = EZRatingView()
    
    var resultData = NSDictionary()
    
    var numberRate = Double()
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Methods
    
    //---------------------------------------------
    
    func setUpview() {
        
        self.setLeftBar(isMenuRequired: false)
        self.setTitle(title: "Write Review")
        self.rateViewConfigure()
        self.SetResultUI()
    }
    
    
    func rateViewConfigure(){
        rateEZ = EZRatingView(frame: (self.rateView?.bounds)!)
        rateEZ.isUserInteractionEnabled = true
        rateEZ.stepInterval = 0.5
        rateEZ.numberOfStar = 5
        rateEZ.addTarget(self, action: #selector(changeRate(sender:)), for: .valueChanged)
        self.rateView?.addSubview(rateEZ)
        
    }
    
       func SetResultUI (){

        
        if let tempStr = resultData.object(forKey: "name") as? String {
            self.lblName.text = tempStr
        }
        
        if let tempStr = resultData.object(forKey: "image") as? String {
            if tempStr.isEmpty == false {
                let imageUrl = URL(string: tempStr)
                self.imgProduct.sd_setImage(with: imageUrl, placeholderImage: UIImage.init())
                
            }
        }
    }
    
    //---------------------------------------------
    
    //MARK:- Action Methods
    
    //---------------------------------------------
    
    @objc func changeRate(sender : EZRatingView) {
        self.numberRate = Double(sender.value)
    }
    
    @IBAction func btnSubmitTapped(_ sender: Any) {
        
        if self.txtView.text == "" || self.txtView.text == String(describing: CharacterSet.whitespacesAndNewlines) {
            self.showAlertWithMessage("Please enter review")
        } else {
            self.callWSReview()
        }
    }
    
    func callWSReview() {
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        let apiName = "product_review&page=add&productID=MQ==&star=\(self.numberRate)&desc=\(self.txtView.text!)&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            if success {
                var helpData:[String : Any] = response as! [String : Any]
                
                self.showAlert(App.AppName, message: helpData["message"] as? String)
                
                self.navigationController?.popViewController(animated: true)
                
            } else {
                self.showAlert(App.AppName, message: response as? String)
            }
        }
        
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpview()
        
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}

