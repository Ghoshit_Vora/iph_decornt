//
//  ProductDetailsVC.swift
//  IPH_Decornt
//
//  Created by xx on 02/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class ProductDetailsVC: BaseVC, DeskBoardCategoryDelegate {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var tblProductList: UITableView!
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var NoOfResultCell = NSMutableArray()
    var productDetailsResult = NSDictionary()
    var specificationDetailList = NSArray()
    
    var productResult = NSDictionary(){
        didSet{
            self.GetProductDetails()
        }
    }
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //---------------------------------------------
    
    //MARK:- Custom Methods
    
    //---------------------------------------------
    
    func setUpview() {
        
        self.setLeftBar(isMenuRequired: false)
        self.setTitle(title: "Product Detail")
        self.setRightData(isSearchBtnRequired: true)
    }
    
    @objc func LoadResultUI(){
        
        print(self.productDetailsResult)
        if self.tblProductList != nil {
            let nibName = UINib(nibName: "ProductDetailsCell", bundle: nil)
            self.tblProductList .register(nibName, forCellReuseIdentifier: "ProductDetailsCell")
            
            let nibName3 = UINib(nibName: "ProductTableCell", bundle: nil)
            self.tblProductList .register(nibName3, forCellReuseIdentifier: "ProductTableCell")
            
            let nibName2 = UINib(nibName: "SpecificationCell", bundle: nil)
            self.tblProductList .register(nibName2, forCellReuseIdentifier: "SpecificationCell")
            
            if let cell = self.tblProductList.dequeueReusableCell(withIdentifier: "ProductDetailsCell") as? ProductDetailsCell {
                cell.delegateSelf = self
                //cell.delegate = self
//                cell.isWishListEnable = self.isWishListEnable
                cell.resultData = self.productDetailsResult
                self.NoOfResultCell.add(cell)
            }
            
            if let cell = self.tblProductList.dequeueReusableCell(withIdentifier: "SpecificationCell") as? SpecificationCell {
                cell.delegate = self
                cell.resultData = self.productDetailsResult
                cell.specificationDetailList = NSArray(array: self.specificationDetailList)
                
//                if let text = cell.reviewText {
//                    inputs.append(text)
//                }
//
//                toolbar = FormToolbar(inputs: self.inputs)
                self.NoOfResultCell.add(cell)
            }
            
            DispatchQueue.main.async
                {
                    
                    self.tblProductList.delegate = self
                    self.tblProductList.dataSource = self
                    self.tblProductList.reloadData()
                    
            }
            
        }else{
            self.perform(#selector(LoadResultUI), with: nil, afterDelay: 0.2)
        }
    }
    
    
    //---------------------------------------------
    
    //MARK:- Action Methods
    
    //---------------------------------------------
    
    @IBAction func btnAddCartTapped(_ sender: Any) {
        self.AddCartProduct(isfromBuyNow: false)
    }
    
    @IBAction func btnBuyNowTapped(_ sender: Any) {
        self.AddCartProduct(isfromBuyNow: true)
    }
    
    //---------------------------------------------
    
    //MARK:- Call WS Methods
    
    //---------------------------------------------
    
    func GetProductDetails() {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        
        print(self.productResult)
        var productId = ""
        if let tempStr = self.productResult.object(forKey: "productID") as? String {
            productId = tempStr
        }
        
        //https://www.decornt.com/mapp/index.php?view=product_detail&productID=OTQ3&custID=MTg=&custPhone=9510069163&phoneType=iphone
        
        let ApiName = "product_detail&productID=\(productId)&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        
        DataManager.sharedInstance.getRequestApi(apiName: ApiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                var productDetailsData:[String : Any] = response as! [String : Any]
                
                if let detail_list = productDetailsData["detail_list"] as? NSArray {
                    self.specificationDetailList = NSArray(array: detail_list)
                }
                
                if let resultList = productDetailsData["product"] as? NSArray {
                    if resultList.count > 0 {
                        if let data = resultList.object(at: 0) as? NSDictionary {
                            self.productDetailsResult = NSDictionary(dictionary: data)
                            DispatchQueue.main.async
                                {
                                    self.LoadResultUI()
                                    self.GetRelatedProduct()
                            }
                        }
                    }
                }
            }
            
            DispatchQueue.main.async
                {
                    
            }
        }
    }
    
    func GetRelatedProduct() {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        
        print(self.productResult)
        var catId = ""
        if let tempStr = self.productDetailsResult.object(forKey: "catID") as? String {
            catId = tempStr
        }
        
        //https://www.decornt.com/mapp/index.php?view=related_product&catID=Mzc=&custID=MTg=&custPhone=9510069163&phoneType=iphone
        
        let ApiName = "related_product&catID=\(catId)&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        
        DataManager.sharedInstance.getRequestApi(apiName: ApiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                var productDetailsData:[String : Any] = response as! [String : Any]
                
                if let related_product = productDetailsData["product_list"] as? NSArray {
                    if related_product.count > 0 {
                        if let cell = self.tblProductList.dequeueReusableCell(withIdentifier: "ProductTableCell") as? ProductTableCell {
                            cell.productTitleLabel.text = "Related Product"
                            cell.ResponseResult = related_product
                            cell.btnViewAll.isHidden = true
                            self.NoOfResultCell.add(cell)
                            
                            DispatchQueue.main.async
                                {
                                    self.tblProductList.delegate = self
                                self.tblProductList.dataSource = self
                            self.tblProductList.reloadData()
                                    
                            }
                        }
                    }
                }
            }
        }
    }
    
    func AddCartProduct(isfromBuyNow : Bool) {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {

            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        
        print(self.productResult)
        var productId = ""
        if let tempStr = self.productResult.object(forKey: "productID") as? String {
            productId = tempStr
        }
        
        var priceId = ""
        var productColor = ""
        if let tempDic = self.productResult.object(forKey: "price_list") as? [[String:Any]] {
            let dictData = tempDic[0]
            
            if let priceIdData = dictData["priceID"] as? String {
                if priceIdData == "" {
                    priceId = "0"
                } else {
                    priceId = priceIdData
                }
                
            } else {
                priceId = "0"
            }
            
            if let productColorData = dictData["colors"] as? String {
                productColor = productColorData
            }
        } else {
            priceId = "0"
        }
        
        
        //https://www.decornt.com/mapp/index.php?view=cart&page=add&productID=MQ==&priceID=MQ==&productQuantity=1&productColor=red&custID=MTg=&custPhone=9510069163&phoneType=iphone
        
        let ApiName = "cart&page=add&productID=\(productId)&priceID=\(priceId)&productQuantity=1&productColor=\(productColor)&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        
        DataManager.sharedInstance.getRequestApi(apiName: ApiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                var productData:[String : Any] = response as! [String : Any]
                UserDefaults.UserData.set(productData["cart_count"] as AnyObject, forKey: UserDefaults.UserData.ObjectDefaultKey.cartCount)
                
                if isfromBuyNow {
                    let objRoot = AppStoryboard.MyCart.viewController(viewControllerClass: MyCartVC.self)
                    self.navigationController?.pushViewController(objRoot, animated: true)
                } else {
                    DispatchQueue.main.async {
                        
                        self.showAlert(App.AppName, message: "Added Successfully")
                    }
                }
                
                
            } else {
                self.showAlert(App.AppName, message: "Product Out Of Stock")
            }
        }
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpview()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}

extension ProductDetailsVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.NoOfResultCell.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let _ = self.NoOfResultCell.object(at: indexPath.row) as? ProductTableCell {
            return 300
        }
       if let _ = self.NoOfResultCell.object(at: indexPath.row) as? SpecificationCell {
            return UITableViewAutomaticDimension
        }
        else {
            return UITableViewAutomaticDimension
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = self.NoOfResultCell.object(at: indexPath.row) as? ProductDetailsCell {
            //cell.delegate = self
            return cell
        }else if let cell = self.NoOfResultCell.object(at: indexPath.row) as? ProductTableCell {
            cell.delegate = self
            return cell
        } else if let cell = self.NoOfResultCell.object(at: indexPath.row) as? SpecificationCell {
            cell.delegate = self
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ProductDetailsVC : ProductDetailsCellDelegate,SpecificationCellDelegate {
    
    func writeReview(cell: SpecificationCell) {
        
        let vc = AppStoryboard.Product.viewController(viewControllerClass: ProductReviewVC.self)
        vc.resultData = self.productDetailsResult
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func viewReviewProductDetails(cell: ProductDetailsCell) {

    }
    func viewReviews(cell: SpecificationCell) {
//        let controller = ReviewsViewController(nibName: "ReviewsViewController", bundle: nil)
//        controller.productDetailsResult = self.productDetailsResult
//        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    func didTapImageView(data: NSDictionary) {
        
        
        if let list = data.object(forKey: "image_list") as? NSArray {
            let objRoot = AppStoryboard.Product.viewController(viewControllerClass: FullZoomImageVC.self)
            objRoot.imageList = NSArray(array: list)
            objRoot.productResult = data
            self.navigationController?.pushViewController(objRoot, animated: true)
        }
        
    }
    
    
    func shareProduct(cell: ProductDetailsCell) {
        print(cell.resultData)
        var share_text = ""
        if let tempStr = cell.resultData.object(forKey: "share_text") as? String {
            share_text = tempStr
        }
        let shareImage = cell.shareImage
        
        
        let vc = UIActivityViewController(activityItems: [share_text, shareImage], applicationActivities: [])
        self.present(vc, animated: true)
    }
    

//    func didLoadCategoryResult(data: NSDictionary) {
//        print(data)
//
////        if data.object(forKey: "productID") != nil {
////            let objRoot = ProductDetailsViewController(nibName: "ProductDetailsViewController", bundle: nil)
////            objRoot.productResult = data
////            self.navigationController?.pushViewController(objRoot, animated: true)
////        }
//    }
    
    //MARK:- TextField & KeyBoard Delegate -
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        toolbar.update()
//        activeInput = textField
        
    }
    
    func reloadWeb() {
        self.tblProductList.reloadData()
    }
    
    func changePinCode() {
        let pinCodeVC = AppStoryboard.Product.viewController(viewControllerClass: PinCodeVC.self)
        
        //AppDelegate.sharedInstance.window?.addSubview((stateListVC.view)!)
        //
        pinCodeVC.modalPresentationStyle = .overCurrentContext
        pinCodeVC.modalTransitionStyle = .crossDissolve
        self.present(pinCodeVC, animated: true, completion: nil)
    }
    
    func didLoadCategoryResult(data: NSDictionary) {
        print(data)
        
        if let subcat = data.object(forKey: "subcat") as? String {
            if subcat.caseInsensitiveCompare("yes") == .orderedSame {
                let viewController = AppStoryboard.Product.viewController(viewControllerClass: CategoryVC.self)
                viewController.homeCategoryResult = data
                self.navigationController?.pushViewController(viewController, animated: true)
                return
            }
        }
        if data.object(forKey: "catID") != nil {
            let objRoot = AppStoryboard.Product.viewController(viewControllerClass: ProductListVC.self)
            objRoot.categoryResultData = data
            self.navigationController?.pushViewController(objRoot, animated: true)
        }else if data.object(forKey: "productID") != nil {
            let objRoot = AppStoryboard.Product.viewController(viewControllerClass: ProductDetailsVC.self)
            objRoot.productResult = data
            self.navigationController?.pushViewController(objRoot, animated: true)
        }
    }
}
