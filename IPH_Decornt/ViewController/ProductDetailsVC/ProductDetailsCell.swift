//
//  ProductDetailsCell.swift
//  Sabzilana
//
//  Created by TNM3 on 3/31/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit
protocol ProductDetailsCellDelegate {
    func didTapImageView(data : NSDictionary)
    func viewReviewProductDetails(cell : ProductDetailsCell)
    func shareProduct(cell : ProductDetailsCell)
}
class ProductDetailsCell: UITableViewCell,MultipleImageHeaderDelegate {

    
    @IBOutlet var scrollview : UIScrollView?
    
    var delegateSelf : ProductDetailsCellDelegate?
    
    var isWishListEnable : Bool = false
    
    @IBOutlet var descLabel : UILabel!
    
    //var delegate : ProductCellBasketUpdateDelegate?
    
    var shareImage = UIImage()
    @IBOutlet var nameLabel : UILabel!
    
    @IBOutlet var priceLabel : UILabel!
   
    @IBOutlet weak var discountLabel: UILabel!
    
    @IBOutlet weak var offerLabel: UILabel!
    
    @IBOutlet var availibilityLabel : UILabel?
    
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblItemCode: UILabel!
    var Qty_Price_ID : String = ""
    var resultData = NSDictionary() {
        didSet{
            self.SetResultUI()
        }
    }
    
    @IBAction func reviewsClick(){
        if self.delegateSelf != nil {
            self.delegateSelf?.viewReviewProductDetails(cell: self)
            
        }
    }
    @IBAction func shareClick(){
        if self.delegateSelf != nil {
            self.delegateSelf?.shareProduct(cell: self)
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ProductDetailsCell.handleTap))
//        self.imageview.isUserInteractionEnabled = true
//        self.imageview.addGestureRecognizer(tapGestureRecognizer)
       
       
        
    }
    func didTapFullScreenImage() {
        self.handleTap()
    }
    func getSahreImage(image: UIImage) {
        self.shareImage = image
    }
    func handleTap(){
        if self.delegateSelf != nil {
            self.delegateSelf?.didTapImageView(data: self.resultData)
        }
    }
    @IBAction func addQtyClick(){
        
        self.UpdateNCheckQty()
        
        //Add Or Update QTY in Basket
//        self.AddProductInBasket()
        
//        if let visibleView = appDelegate.window?.visibleViewController?.view {
//            visibleView.makeToast(kQtyAddedMessage, duration: 1.0, position: CSToastPositionBottom)
//        }
        
    }
    @IBAction func plusQtyClick(){
        
        self.UpdateNCheckQty()
        
        //Add Or Update QTY in Basket
//        self.AddProductInBasket()
        
//        if let visibleView = appDelegate.window?.visibleViewController?.view {
//            visibleView.makeToast(kQtyUpdatedMessage, duration: 1.0, position: CSToastPositionBottom)
//        }
    }
    @IBAction func minusQtyClick(){
        
        self.UpdateNCheckQty()
        
        //Add Or Update QTY in Basket
//        self.AddProductInBasket()
        
//        if let visibleView = appDelegate.window?.visibleViewController?.view {
//            visibleView.makeToast(kQtyUpdatedMessage, duration: 1.0, position: CSToastPositionBottom)
//        }
    }
    func UpdateNCheckQty(){
//        if self.selectedQty <= 0 {
//            self.selectedQty = 0
////            self.buttonAddQty.isHidden = false
////            self.qtyView.isHidden = true
//        }else{
////            self.buttonAddQty.isHidden = true
////            self.qtyView.isHidden = false
//
//        }
//        self.qtyLabel.text = String(format: "%d", self.selectedQty)
        
    }
    
    @objc func SetResultUI(){
        
        if self.nameLabel != nil && self.resultData.object(forKey: "productID") != nil {
            
            if let scroll = self.scrollview {
                let multipleImageView = MultipleImageHeader.instanceFromNib()
                multipleImageView.delegate = self
                multipleImageView.frame = scroll.bounds
                multipleImageView.contentMode = .scaleAspectFit
                scroll.addSubview(multipleImageView)
                
                if let image_list = resultData.object(forKey: "image_list") as? NSArray {
                    multipleImageView.imageList = image_list
                }
            }
            
            
            
            if let tempStr = resultData.object(forKey: "name") as? String {
                self.nameLabel.text = tempStr
            }
            
    
            if let tempDic = resultData.object(forKey: "price_list") as? [[String:Any]] {
                
                if tempDic.count > 0 {
                    
                    let dictData = tempDic[0]
                    
                    let priceStr = "₹ \(dictData["price"] as! String)"
                    
                    self.priceLabel.text = priceStr
                    
                    let mrpStr = "₹ \(dictData["mrp"] as! String)"
                    self.discountLabel.text = mrpStr
                    let attributedString = NSMutableAttributedString(string: self.discountLabel.text!)
                    attributedString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 2, range: NSMakeRange(0, attributedString.length))
                    self.discountLabel.attributedText = attributedString
                    
                    let dicountStr = "\(dictData["discount"] as! String)% off"
                    self.lblDiscount.text = dicountStr
                    
                    if let strOffer = dictData["cashback_text"] as? String {
                        self.offerLabel.text = strOffer
                    }
                }
                
            }
        
            
            if let tempStr = resultData.object(forKey: "qty_message") as? String {
                self.availibilityLabel?.text = tempStr
            }
            
            if let tempStr = resultData.object(forKey: "model") as? String {
                self.lblItemCode?.text = tempStr
            }
           
            
        }else{
            self.perform(#selector(ProductDetailsCell.SetResultUI))
        }
        
    }
    
    //Update GM or PCS
    func UpdateGMOrPcsQty(priceData : NSDictionary){
        if let tempStr = priceData.object(forKey: "price_ID") as? String {
            self.Qty_Price_ID = tempStr
        }
        
        
        if let tempStr = priceData.object(forKey: "price") as? String {
            let priceStr = String(format: "₹ %@", arguments: [tempStr])
            self.priceLabel.text = priceStr
            
        }
        
        self.UpdateNCheckQty()
        
    }
    
    //MARK:- Item Pack Size -
    @IBAction func ItemsPackSizeClick(){
        
//        var viewController = UIViewController()
//
//        if let tempController = appDelegate.window?.visibleViewController {
//            viewController = tempController
//        }
//        let popup = PopupController
//            .create(viewController)
//            .customize(
//                [
//                    .layout(.center),
//                    .animation(.fadeIn),
//                    .backgroundStyle(.blackFilter(alpha: 0.8)),
//                    .dismissWhenTaps(true),
//                    .scrollable(false)
//                ]
//            )
//            .didShowHandler { popup in
//                print("showed popup!")
//            }
//            .didCloseHandler { popup in
//                print("closed popup!")
//        }
//
//        let container = ItemPackSizeController.instance()
//        container.delegate = self
//
//        if let priceList = resultData.object(forKey: "price_list") as? NSArray {
//            container.responseResult = NSArray(array: priceList)
//        }
//
//        container.closeHandler = { data in
//            popup.dismiss()
//        }
//
//        popup.show(container)
    }
    
    //MAR:- Dropdown delegate -
    func didSelectDropDownResult(data: AnyObject) {
        print(data)
        if let priceData = data as? NSDictionary {
            DispatchQueue.main.async {
                self.UpdateGMOrPcsQty(priceData: priceData)
            }
        }
        
    }
    
    //MARK:- Manage Favourite -
    @IBAction func FavouriteClick(){
        
        //MARK: - If User Not Logged in Sabzilana App -
//        if appDelegate.SabzilanaLoginUserId.isEmpty == true {
//            appDelegate.LoginRequired(message: "You need to be signed in to Create and Save your own wishlist.")
//            return
//        }
//
//        if let tempStr = self.resultData.object(forKey: "productID") as? String {
//            let queryHomeStr = String(format: "SELECT * FROM %@ WHERE %@ = \"%@\"", kWishListTableName,kWishListProductID,tempStr)
//
//            let dbResult = MyDbManager.sharedClass().exicuteSQLiteQuery(queryHomeStr)
//
//            if let wishListResult = dbResult {
//                if wishListResult.count > 0 {
//                    var productId = ""
//
//                    if let dbData = wishListResult.object(at: 0) as? NSDictionary {
//                        //DB Data
//                        if let tempStr = dbData.object(forKey: kWishListProductID) as? String {
//                            productId = tempStr
//                        }
//
//                        //Delete Query
//                        let queryDelete = String(format: "DELETE FROM %@ WHERE %@ = \"%@\"", kWishListTableName,kWishListProductID,productId)
//                        MyDbManager.sharedClass().exicuteSQLiteQuery(queryDelete)
//                    }
//                }else{
//                    if tempStr.isEmpty == false {
//                        let insertData = [kWishListProductID : tempStr]
//                        MyDbManager.sharedClass().insert(into: kWishListTableName, field: insertData )
//                    }
//                }
//            }
//
//            DispatchQueue.main.async {
//                self.FavouriteUpdate()
//            }
//        }
    }
    func FavouriteUpdate(){
        //        favourites
        /*
        if let tempStr = self.resultData.object(forKey: "productID") as? String {
            let queryHomeStr = String(format: "SELECT * FROM %@ WHERE %@ = \"%@\"", kWishListTableName,kWishListProductID,tempStr)
            
            let dbResult = MyDbManager.sharedClass().exicuteSQLiteQuery(queryHomeStr)
            
            if let wishListResult = dbResult {
                if wishListResult.count > 0 {
                    self.btnFavourite.setImage(#imageLiteral(resourceName: "favouriteSel"), for: .normal)
                }else{
                    if tempStr.isEmpty == false {
                        self.btnFavourite.setImage(#imageLiteral(resourceName: "favourites"), for: .normal)
                    }
                }
            }
        }
         */
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
