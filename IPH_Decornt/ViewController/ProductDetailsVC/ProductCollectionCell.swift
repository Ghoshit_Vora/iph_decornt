//
//  ProductCollectionCell.swift
//  Sabzilana
//
//  Created by TNM3 on 3/29/17.
//  Copyright © 2017 Sabzilana. All rights reserved.
//

import UIKit

class ProductCollectionCell: UICollectionViewCell {

    @IBOutlet var containerView : UIView!
    
    @IBOutlet var imageview : UIImageView!
    @IBOutlet var nameLabel : UILabel!
    @IBOutlet var discountLabel : UILabel!
    @IBOutlet var priceLabel : UILabel!
    @IBOutlet var mrpLabel : UILabel!
    
    @IBOutlet var btnFavourite : UIButton!
    
    
    var resultData = NSDictionary(){
        didSet{
            self.SetResultUI()
        }
    }
    func SetResultUI(){
        if self.imageview != nil {
            if let tempStr = resultData.object(forKey: "image") as? String {
                if tempStr.isEmpty == false {
                    let imageUrl = URL(string: tempStr)
                    self.imageview.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                        if image != nil {
                            self.imageview.image = image
                        }
                    })
                    
                }
            }
            print(resultData)
            var nameStr = ""
            if let tempStr = resultData.object(forKey: "name") as? String {
                nameStr = tempStr
            }
            if let tempStr = resultData.object(forKey: "caption") as? String {
                nameStr = String(format: "%@\n%@", arguments: [nameStr,tempStr])
            }
            self.nameLabel.text = nameStr
            
            if let tempStr = resultData.object(forKey: "discount") as? String {
                if tempStr.isEmpty == true {
                    self.discountLabel.isHidden = true
                }else{
                    self.discountLabel.isHidden = false
                    self.discountLabel.text = String(format: "%@", arguments: [tempStr])
                }
                
            }else {
                if let tempStr = resultData.object(forKey: "discount") as? NSNumber {
                    if tempStr == 0 {
                        self.discountLabel.isHidden = true
                    }else{
                        self.discountLabel.isHidden = false
                        self.discountLabel.text = String(format: "%@", arguments: [tempStr.stringValue])
                    }
                    
                }
            }
            if let tempStr = resultData.object(forKey: "mrp") as? String {
                if tempStr.isEmpty == false {
                    self.mrpLabel.isHidden = false
                    self.mrpLabel.text = String(format: "₹ %@", arguments: [tempStr])
                    
                    let attributedString = NSMutableAttributedString(string: self.mrpLabel.text!)
                    attributedString.addAttribute(NSAttributedStringKey.strikethroughStyle,  value: 1, range: NSMakeRange(0, attributedString.length))
                    self.mrpLabel.attributedText = attributedString
                }else{
                    self.mrpLabel.isHidden = true
                }
                
                
            }
            if let tempStr = resultData.object(forKey: "price") as? String {
                
                self.priceLabel.text = String(format: "₹ %@", arguments: [tempStr])
                
            }
            
        }else{
            //self.perform(#selector(HeaderCollectionCell.SetResultUI))
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.containerView.layer.cornerRadius = 5
        self.containerView.layer.borderColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 0.8).cgColor
        self.containerView.layer.borderWidth = 1
        
    }

    //MARK:- Manage Favourite -
    @IBAction func FavouriteClick(){
        //MARK: - If User Not Logged in Sabzilana App -
        
    }
    
}
