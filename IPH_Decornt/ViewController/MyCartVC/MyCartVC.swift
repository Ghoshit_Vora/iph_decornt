//
//  MyCartVC.swift
//  IPH_Decornt
//
//  Created by xx on 16/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit
import FTPopOverMenu_Swift

class MyCartVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var tblCart: UITableView!
    
    @IBOutlet weak var lblBottomAmount: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblAmoutPayble: UILabel!
    
    @IBOutlet weak var lblItemCount: UILabel!
   
    @IBOutlet var viewPrice: UIView!
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var arrCartData:[[String : Any]] = []
    var totalAmount = Double()
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Methods
    
    //---------------------------------------------
    
    func setUpView() {
        
        self.setTitle(title: "My Cart")
        
        let configuration = FTConfiguration.shared
        configuration.backgoundTintColor = UIColor.white
        configuration.textColor = UIColor.black
        
        self.setLeftBar(isMenuRequired: true)
        self.setRightData(isSearchBtnRequired: true)
        
        self.tblCart.tableFooterView = self.viewPrice
    }
    
    //---------------------------------------------
    
    //MARK:- Action Methods
    
    //---------------------------------------------
    
    @objc func btnQtyTapped(sender : UIButton) {
        let data = arrCartData[sender.tag]
        
        var maxQty = Int()
            
        maxQty = Int(data["maxqty"] as! String)!
        
        var arrQtyCount:[String] = []
        
        for var i in 1...maxQty {
            arrQtyCount.append("Qty: \(i)")
        }
        
        FTPopOverMenu.showForSender(sender: sender,
                                    with: arrQtyCount,
                                    done: { (selectedIndex) -> () in
                                        
        sender.setTitle(arrQtyCount[selectedIndex], for: .normal)
                                        
        let qty = selectedIndex + 1
                                        
        self.callUpdateQTYWS(productId: data["productID"] as! String, priceId: data["priceID"] as! String, cartId: data["cartID"] as! String, qty: String(qty))
                                        
        }) {
            
        }
    }
    
    @objc func btnMoveToWishListTapped(sender : UIButton)  {
        
    }
    
    @objc func btnRemoveTapped(sender : UIButton)   {
        
        let data = arrCartData[sender.tag]
        
        self.callRemoveWS(productId: data["productID"] as! String, priceId: data["priceID"] as! String, cartId: data["cartID"] as! String)
    }
    
    
    @IBAction func btnContinueTapped(_ sender: Any) {
        
        if self.arrCartData.count > 0 {
            let vc = AppStoryboard.MyCart.viewController(viewControllerClass: ShoppingAddressVC.self)
            vc.strItemCount = self.lblItemCount.text!
            vc.strPrice = self.lblAmoutPayble.text!
            vc.totalAmount = self.totalAmount
            vc.totalItem = self.arrCartData.count
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
            self.showAlertWithTitle("cart is empty!")
        }
    }
    
    //---------------------------------------------
    
    //MARK:- WS Methods
    
    //---------------------------------------------
    
    func callMyCartWS() {
        
//        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
//            
//            return
//        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        let apiName = "cart&page=list&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                let myCartData:[String : Any] = response as! [String : Any]
                
                self.arrCartData = myCartData["cart"] as! [[String : Any]]
                
                self.lblItemCount.text = "Price (\(self.arrCartData.count) Items)"
                
                self.totalAmount = Double(myCartData["amount_total"] as! String)!
                self.lblBottomAmount.text = "₹ \(myCartData["amount_total"] as! String)"
                self.lblPrice.text = "₹ \(myCartData["amount_total"] as! String)"
                self.lblAmoutPayble.text = "₹ \(myCartData["amount_total"] as! String)"
                self.tblCart.reloadData()
            }
            
        }
    }
    
    func callMoveToWishListWS() {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        //https://www.decornt.com/mapp/index.php?view=cart&page=list&custID=MTg=&custPhone=9510069163&phoneType=iphone
        let apiName = "cart&page=list&custID=MTg=&custPhone=9510069163&phoneType=iphone"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                self.callMyCartWS()
            }
            
        }
    }
    
    
    func callRemoveWS(productId : String, priceId : String, cartId : String) {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        let apiName = "cart&page=remove&productID=\(productId)&priceID=\(priceId)&cartID=\(cartId)&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                self.callMyCartWS()
            }
            
        }
    }
    
    func callUpdateQTYWS(productId : String, priceId : String, cartId : String, qty : String) {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        //https://www.decornt.com/mapp/index.php?view=cart&page=update&productID=OTQ3&priceID=0&productQuantity=2&cartID=5052&custID=MTg=&custPhone=9510069163&phoneType=iphone
        let apiName = "cart&page=update&productID=\(productId)&priceID=\(priceId)&productQuantity=\(qty)&cartID=\(cartId)&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                self.callMyCartWS()
            }
            
        }
    }
    
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.callMyCartWS()
    }
    
    //---------------------------------------------

}

extension MyCartVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrCartData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomMyCartCell") as! CustomMyCartCell
        
        cell.setUpData(dictData: arrCartData[indexPath.row])
        cell.btnQty.tag = indexPath.row
        cell.btnRemove.tag = indexPath.row
        cell.btnMoveToWishList.tag = indexPath.row
        
        cell.btnQty.addTarget(self, action: #selector(btnQtyTapped), for: .touchUpInside)
        
        cell.btnRemove.addTarget(self, action: #selector(btnRemoveTapped), for: .touchUpInside)
        
        cell.btnMoveToWishList.addTarget(self, action: #selector(btnMoveToWishListTapped), for: .touchUpInside)
        
        return cell
    }
    
}
