//
//  AddMyAddressVC.swift
//  IPH_Decornt
//
//  Created by xx on 15/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class AddMyAddressVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var txtfullName: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtAdreesLine1: UITextField!
    @IBOutlet weak var txtAddressLine2: UITextField!
    @IBOutlet weak var txtArea: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtPinCode: UITextField!
    
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var isFromEdit: Bool = false
    var stateId = String()
    var dictData:[String : Any] = [:]
    var addressId = String()
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Method
    
    //---------------------------------------------
    
    func setUpView() {
        
        if isFromEdit {
            self.setTitle(title: "Edit Address")
            self.setupdata()
        } else {
            self.setTitle(title: "Add Address")
        }
        
        self.setLeftBar(isMenuRequired: false)
    }
    
    func setupdata() {
        
        self.txtfullName.text = dictData["name"] as? String
        self.txtPhoneNumber.text = dictData["phone"] as? String
        self.txtAdreesLine1.text = dictData["address1"] as? String
        self.txtAddressLine2.text = dictData["address2"] as? String
        self.txtArea.text = dictData["area"] as? String
        self.txtCity.text = dictData["city"] as? String
        self.txtState.text = dictData["state"] as? String
        self.txtPinCode.text = dictData["pincode"] as? String
        self.addressId = dictData["addID"] as! String
    }
    
    //---------------------------------------------
    
    //MARK:- Action Method
    
    //---------------------------------------------
    
    @IBAction func btnAddAddressTapped(_ sender: Any) {
        
        let validator = Validator()
        validator.registerField(self.txtfullName, rule: [RequiredRule(message: "Please enter name")])
        
        validator.registerField(self.txtAdreesLine1, rule: [RequiredRule(message: "Please address line")])
        validator.registerField(self.txtAddressLine2, rule: [RequiredRule(message: "Please address name")])
        validator.registerField(self.txtArea, rule: [RequiredRule(message: "Please enter area")])
        validator.registerField(self.txtCity, rule: [RequiredRule(message: "Please enter city")])
        validator.registerField(self.txtState, rule: [RequiredRule(message: "Please enter state")])
        validator.registerField(self.txtPinCode, rule: [RequiredRule(message: "Please enter pincode")])
        validator.registerField(self.txtPhoneNumber, rule: [RequiredRule(message: "Please enter phone number"), PhoneNumberRule(message: "Please enter valid phone number") ])
        
        
        validator.validate({
            
            self.callWSAddAddress()
        }) { (arrValidationError) in
            
            if let firstError = arrValidationError.first {
                self.showAlertWithMessage(firstError.errorMessage)
            }
        }
        
    }
    
    @IBAction func btnStateSelectedTapped(_ sender: Any) {
        
        let stateListVC = AppStoryboard.Authotication.viewController(viewControllerClass: StatedListVC.self)
          stateListVC.delegate = self
     //AppDelegate.sharedInstance.window?.addSubview((stateListVC.view)!)
       //
        stateListVC.modalPresentationStyle = .overCurrentContext
        stateListVC.modalTransitionStyle = .crossDissolve
        self.present(stateListVC, animated: true, completion: nil)
    }
    
    //---------------------------------------------
    
    //MARK:- WS Method
    
    //---------------------------------------------
    
    func callWSAddAddress() {
        
//        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
//            
//            return
//        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        //https://www.decornt.com/mapp/index.php?view=addresses&page=add&cname=zx&cphone=1323&caddress1=asdas&caddress2=asds&carea=121sa&ccity=asdas&cstate=asda&cpincode=132&custID=MTg=&custPhone=9510069163&phoneType=iphone
        
        //https://www.decornt.com/mapp/index.php?view=addresses&page=add&caddressID=MQ==&cname=zx&cphone=1323&caddress1=asdas&caddress2=asds&carea=121sa&ccity=asdas&cstate=asda&cpincode=132&custID=MTg=&custPhone=9510069163&phoneType=iphone
        var apiName = ""
        if isFromEdit {
            apiName = "addresses&page=add&caddressID=\(self.addressId)&cname=\(self.txtfullName.text!)&cphone=\(self.txtPhoneNumber.text!)&caddress1=\(self.txtAdreesLine1.text!)&caddress2=\(self.txtAddressLine2.text!)&carea=\(self.txtArea.text!)&ccity=\(self.txtCity.text!)&cstate=\(self.txtState.text!)&cpincode=\(self.txtPinCode.text!)&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        } else {
            apiName = "addresses&page=add&cname=\(self.txtfullName.text!)&cphone=\(self.txtPhoneNumber.text!)&caddress1=\(self.txtAdreesLine1.text!)&caddress2=\(self.txtAddressLine2.text!)&carea=\(self.txtArea.text!)&ccity=\(self.txtCity.text!)&cstate=\(self.txtState.text!)&cpincode=\(self.txtPinCode.text!)&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        }
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                self.navigationController?.popViewController(animated: true)
            }
            
        }
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}

extension AddMyAddressVC:StateListViewDelegate {
    func didSelected(state: [String : Any]) {
        
        self.txtState.text = state["name"] as? String
        self.stateId = (state["SR"] as? String)!
    }
    
    
}
