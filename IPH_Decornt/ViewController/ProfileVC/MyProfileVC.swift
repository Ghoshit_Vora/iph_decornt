//
//  MyProfileVC.swift
//  IPH_Decornt
//
//  Created by xx on 28/08/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class MyProfileVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblMobile: UILabel!
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Methods
    
    //---------------------------------------------
    
    func setUpView() {
    
        self.setLeftBar(isMenuRequired: false)
        
        if let name = UserDefaults.UserData.object(forKey: .name) as? String {
            self.lblName.text = name
        }
        
        if let mobile = UserDefaults.UserData.object(forKey: .phone) as? String {
            self.lblMobile.text = mobile
        }
        
        if let email = UserDefaults.UserData.object(forKey: .email) as? String {
            self.lblEmail.text = email
        }
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}
