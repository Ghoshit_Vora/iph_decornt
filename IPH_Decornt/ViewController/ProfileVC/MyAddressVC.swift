//
//  MyAddressVC.swift
//  IPH_Decornt
//
//  Created by xx on 28/08/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit
import FTPopOverMenu_Swift

class MyAddressVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var tblAddressList: UITableView!
    @IBOutlet weak var lblAddressCount: UILabel!
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var arrAddressData:[[String : Any]] = []
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Methods
    
    //---------------------------------------------
    
    func setUpView() {
        self.setTitle(title: "My Address")
        
        let configuration = FTConfiguration.shared
        configuration.backgoundTintColor = UIColor.white
        configuration.textColor = UIColor.black
    }
    
    //---------------------------------------------
    
    //MARK:- Action Methods
    
    //---------------------------------------------
    
    @IBAction func btnAddAddressTapped(_ sender: Any) {
        let addAddAddressVC = AppStoryboard.Authotication.viewController(viewControllerClass: AddMyAddressVC.self)
        self.navigationController?.pushViewController(addAddAddressVC, animated: true)
    }
    
    
    @objc func btnAddessMenuTapped(sender : UIButton) {
       
        FTPopOverMenu.showForSender(sender: sender,
                                    with: ["Edit", "Delete"],
                                    done: { (selectedIndex) -> () in
                     
                                        if selectedIndex == 0 {
                                            let addAddAddressVC = AppStoryboard.Authotication.viewController(viewControllerClass: AddMyAddressVC.self)
                                           
                                           addAddAddressVC.isFromEdit = true
                                           
                                            addAddAddressVC.dictData = self.arrAddressData[sender.tag]
                                            self.navigationController?.pushViewController(addAddAddressVC, animated: true)
                                        } else {
                                            let data = self.arrAddressData[sender.tag]
                                            
                                            let id = data["addID"] as! String
                                            self.callDeleteWS(addressId: id)
                                        }
                                        
        }) {
            
        }
        
        
    }
    
    //---------------------------------------------
    
    //MARK:- WS Methods
    
    //---------------------------------------------
    
    func callMyAddressWS() {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        //https://www.decornt.com/mapp/index.php?view=addresses&page=list&custID=MTg=&custPhone=9510069163&phoneType=iphone
        let apiName = "addresses&page=list&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                let myAddressData:[String : Any] = response as! [String : Any]
                
                if let flag = myAddressData["flag"] as? String {
                    if flag != "no_address" {
                        self.arrAddressData = myAddressData["address_list"] as! [[String : Any]]
                        
                        self.lblAddressCount.text = "\(self.arrAddressData.count) SAVED ADDRESSES"
                        self.tblAddressList.reloadData()
                    }
                } else {
                    self.arrAddressData = myAddressData["address_list"] as! [[String : Any]]
                    
                    self.lblAddressCount.text = "\(self.arrAddressData.count) SAVED ADDRESSES"
                    self.tblAddressList.reloadData()
                }
            }
        }
    }
    
    func callDeleteWS(addressId : String) {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        //https://www.decornt.com/mapp/index.php?view=addresses&page=remove&caddressID=MQ==&custID=MTg=&custPhone=9510069163&phoneType=iphone
        let apiName = "addresses&page=remove&caddressID=\(addressId)&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                self.callMyAddressWS()
            }
            
        }
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
       setUpView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.callMyAddressWS()
    }
    
    //---------------------------------------------

}

extension MyAddressVC : UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrAddressData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAddreessCellTableViewCell") as! MyAddreessCellTableViewCell
        
        cell.setUpData(dict: arrAddressData[indexPath.row])
        cell.btnAddressMenu.tag = indexPath.row
        cell.btnAddressMenu.addTarget(self, action: #selector(btnAddessMenuTapped), for: .touchUpInside)
        return cell
    }
    
}
