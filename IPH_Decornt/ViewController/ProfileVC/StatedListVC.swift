//
//  StatedListVC.swift
//  IPH_Decornt
//
//  Created by xx on 15/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

protocol StateListViewDelegate: class {
    
    func didSelected(state: [String : Any])
    
}

class StatedListVC: UIViewController {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var tblStateList: UITableView!
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var delegate: StateListViewDelegate?
    var arrStateData:[[String : Any]] = []
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    //---------------------------------------------
    
    //MARK:- Action Methods
    
    //---------------------------------------------
    
    @IBAction func btnCloseTapped(_ sender: Any) {
    
        AppDelegate.sharedInstance.window?.removeFromSuperview()
    }
    
    //---------------------------------------------
    
    //MARK:- WS Methods
    
    //---------------------------------------------
    
    func callGetStateWS() {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
       
        let apiName = "state&page=list&custID=\(strCustId)&custPhone=\(strCustPhone)&phoneType=iphone"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                let stateData:[String : Any] = response as! [String : Any]
                
                if self.arrStateData.count == 0 {
                    self.arrStateData = stateData["list"] as! [[String : Any]]
                    self.tblStateList.dataSource = self
                    self.tblStateList.delegate = self
                    self.tblStateList.reloadData()
                }
                
            }
            
        }
    }
    
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
//            // Put your code which should be executed with a delay here
//            DispatchQueue.main.async {
//                self.callGetStateWS()
//            }
//
//        })
        
        self.callGetStateWS()
        
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //self.callGetStateWS()
    }

}

extension StatedListVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrStateData.count
    }
    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomStateListCell") as! CustomStateListCell
//
//        let data = self.arrStateData[indexPath.row]
//        cell.lblState.text = data["name"] as? String
//
//        return cell
//    }
    
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomStateListCell") as! CustomStateListCell
        
        let data = self.arrStateData[indexPath.row]
        cell.lblState.text = data["name"] as? String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = self.arrStateData[indexPath.row]
        self.delegate?.didSelected(state: data)
        self.dismiss(animated: true, completion: nil)
    }
    
}
