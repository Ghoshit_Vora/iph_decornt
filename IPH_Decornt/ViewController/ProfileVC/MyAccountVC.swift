//
//  MyAccountVC.swift
//  IPH_Decornt
//
//  Created by xx on 28/08/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class MyAccountVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Methods
    
    //---------------------------------------------
    
    func setUpView() {
        self.setTitle(title: "My Account")
        self.setLeftBar(isMenuRequired: true)
        
        if let name = UserDefaults.UserData.object(forKey: .name) as? String {
            self.lblName.text = name
        }
        
        if let mobile = UserDefaults.UserData.object(forKey: .phone) as? String {
            self.lblMobile.text = mobile
        }
    }
    
    //---------------------------------------------
    
    //MARK:- Action Methods
    
    //---------------------------------------------
    
    @IBAction func btnMyAccountTapped(_ sender: Any) {
        let myProfileVC = AppStoryboard.Authotication.viewController(viewControllerClass: MyProfileVC.self)
        
        self.navigationController?.pushViewController(myProfileVC, animated: true)
        
    }
    
    
    @IBAction func btnMyOrderTapped(_ sender: Any) {
        
        let orderVC = AppStoryboard.Product.viewController(viewControllerClass: MyOrderVC.self)
        orderVC.isFromMenu = false
        AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: orderVC)
    }
    
    
    @IBAction func btnWalletHistoryTapped(_ sender: Any) {
        
        let myWalletVC = AppStoryboard.MyWallet.viewController(viewControllerClass: MyWalletVC.self)
        
        self.navigationController?.pushViewController(myWalletVC, animated: true)
    }
    
    
    @IBAction func btnMyWishListTapped(_ sender: Any) {
    }
    
    
    @IBAction func btnMyAddressTapped(_ sender: Any) {
        let myAddressVC = AppStoryboard.Authotication.viewController(viewControllerClass: MyAddressVC.self)
        
        self.navigationController?.pushViewController(myAddressVC, animated: true)
    }
    
    
    @IBAction func btnNotificationTapped(_ sender: Any) {
        let NotificaionVC = AppStoryboard.Notification.viewController(viewControllerClass: NotificationVC.self)
        
      self.navigationController?.pushViewController(NotificaionVC, animated: true)
    }
    
    
    @IBAction func btnDeActiveTapped(_ sender: Any) {
    }
    
    
    @IBAction func btnSignOutTapped(_ sender: Any) {
        
        let actionOK = UIAlertAction(title: "Ok", style: .default) { (action) in
            let homeVC = AppStoryboard.Authotication.viewController(viewControllerClass: LoginVC.self)
            let navigationController = UINavigationController(rootViewController: homeVC)
            AppDelegate.sharedInstance.window?.rootViewController = navigationController
            // UserDefaults.Account.set(false, forKey: UserDefaults.Account.BoolDefaultKey.isUserLoggedIn)
        }
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        self.showAlert(App.AppName, message: "Do you want to logout?", alertActions: [actionOK, actionCancel])
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}
