//
//  MyWalletVC.swift
//  IPH_Decornt
//
//  Created by xx on 26/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class MyWalletVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var tblWallet: UITableView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblDecorntWalletAmount: UILabel!
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var isFromMenu: Bool = true
    var walletResul = NSMutableArray()
    var walletBalance : String = ""
    var PAGING_LIMIT : Int = 10
    var currentPageCode : Int = 0
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Methods
    
    //---------------------------------------------
    
    func setUpView() {
        
        self.setTitle(title: "My Wallet")
        
        if isFromMenu {
            self.setLeftBar(isMenuRequired: true)
        } else {
            self.setLeftBar(isMenuRequired: false)
        }
        
        let nibName = UINib(nibName: "WalletHistoryCell", bundle: nil)
        self.tblWallet .register(nibName, forCellReuseIdentifier: "WalletHistoryCell")
        
        self.callMyCartWS(pageCode: 0)
       
    }
    
    
    @objc func loadMore() {
        
        self.currentPageCode = self.currentPageCode + 1
        self.callMyCartWS(pageCode: self.currentPageCode)

    }
    //---------------------------------------------
    
    //MARK:- WS Methods
    
    //---------------------------------------------
    
    func callMyCartWS(pageCode : Int) {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        var strCustPhone = String()
        
        if let phone = UserDefaults.UserData.object(forKey: .phone) as? String {
            strCustPhone = phone
        }
        
        if pageCode <= 0 {
            AppDelegate.sharedInstance.startLoadingIndicator()
            self.walletResul = NSMutableArray()
        }
        
        //https://www.decornt.com/mapp/index.php?view=wallet_transction&userID=MTg=&userPhone=9510069163&pagecode=0
        let apiName = "wallet_transction&userID=\(strCustId)&userPhone=\(strCustPhone)&pagecode=\(pageCode)"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
                let myWalletData:[String : Any] = response as! [String : Any]
                
                if let tempStr = myWalletData["super_walletBal"] as? String {
                    DispatchQueue.main.async
                        {
                            //self.walletBalance = tempStr
                            if tempStr.caseInsensitiveCompare("N/A") == .orderedSame {
                                self.lblAmount.text = tempStr
                            }else{
                                self.lblAmount.text = String(format: "₹ %@", arguments: [tempStr])
                                
                            }
                    }
                }
                
                if let tempStr = myWalletData["walletBal"] as? String {
                    DispatchQueue.main.async
                        {
                            //self.walletBalance = tempStr
                            if tempStr.caseInsensitiveCompare("N/A") == .orderedSame {
                                self.lblDecorntWalletAmount.text = tempStr
                            }else{
                                self.lblDecorntWalletAmount.text = String(format: "₹ %@", arguments: [tempStr])
                                
                            }
                    }
                }
                
                
                if let resultList = myWalletData["transction_data"] as? NSArray {
                    //                        self.ProductListResul = NSMutableArray(array: resultList)
                    
                    for tempData in resultList {
                        self.walletResul.add(tempData)
                    }
                    DispatchQueue.main.async
                        {
                            if pageCode <= 0 {
                                self.tblWallet.delegate = self
                                self.tblWallet.dataSource = self
                                self.tblWallet.reloadData()
                            }else{
                                self.tblWallet.reloadData()
                            }
                    }
                    
                }
            }
            
        }
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
       setUpView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}

extension MyWalletVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.walletResul.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.walletResul.count > indexPath.row {
            if let data = self.walletResul.object(at: indexPath.row) as? NSDictionary {
                if let cell = self.tblWallet.dequeueReusableCell(withIdentifier: "WalletHistoryCell") as? WalletHistoryCell {
                    cell.resultData = data
                    
                    if self.walletResul.count >= PAGING_LIMIT {
                        if indexPath.row == (self.walletResul.count - 1) {
                            
                            self.loadMore()
                            
                        }
                    }
                    return cell
                }
            }
            
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
}
