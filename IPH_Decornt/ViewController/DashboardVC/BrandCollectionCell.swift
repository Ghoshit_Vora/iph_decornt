//
//  BrandCollectionCell.swift
//  IPH_Decornt
//
//  Created by xx on 27/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class BrandCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
    
    @IBOutlet weak var viewOuter: UIView!
    @IBOutlet weak var btnBrand: UIButton!
    
    override func awakeFromNib() {
        
        self.viewOuter.setCardView(view: self.viewOuter)
    }
}
