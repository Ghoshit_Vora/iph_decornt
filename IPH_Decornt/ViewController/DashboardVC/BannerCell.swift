//
//  BannerCell.swift
//  IPH_Decornt
//
//  Created by xx on 26/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class BannerCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    var bannerStr = String()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setImageData()  {
        let imageUrl = URL(string:bannerStr)
        self.imgView.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
            if image != nil {
                self.imgView.image = image
            }
        })
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
