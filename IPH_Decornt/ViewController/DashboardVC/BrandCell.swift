//
//  BrandCell.swift
//  IPH_Decornt
//
//  Created by xx on 27/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class BrandCell: UITableViewCell,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    var delegate : DeskBoardCategoryDelegate?
    
    var ResponseResult = NSArray() {
        didSet{
            self.LoadResult()
        }
    }
    
    
    @objc func LoadResult(){
        if self.collectionView != nil {
            DispatchQueue.main.async
                {
                    
                    
                    self.collectionView.delegate = self
                    self.collectionView.dataSource = self
                    self.collectionView.reloadData()
                    
                   
            }
        }else{
            
        }
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        let nibName5 = UINib(nibName: "BrandCollectionCell", bundle:nil)
        self.collectionView.register(nibName5, forCellWithReuseIdentifier: "BrandCollectionCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - UICollectionView Delegate
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.ResponseResult.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.ResponseResult.count > indexPath.row {
            if let result = self.ResponseResult.object(at: indexPath.row) as? NSDictionary {
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BrandCollectionCell", for: indexPath) as? BrandCollectionCell {
                    
                    if let tempStr = result.object(forKey: "image") as? String {
                        if tempStr.isEmpty == false {
                            let imageUrl = URL(string:tempStr)
                            cell.imgView.sd_setImage(with: imageUrl, completed: { (image, error, case, url) in
                                if image != nil {
                                    cell.imgView.image = image
                                }
                            })
                            
                        }
                        
                    }
                    return cell
                }
            }
        }
        
        return UICollectionViewCell()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 123, height: 117)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let data = ResponseResult[indexPath.row]
        if self.delegate != nil {
           
                self.delegate?.didLoadCategoryResult(data: data as! NSDictionary)
            
        }
    }
    
    @objc func btnBrandTapped(sender : UIButton) {
        
    }

}
