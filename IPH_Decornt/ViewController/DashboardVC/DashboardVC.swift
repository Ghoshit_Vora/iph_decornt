//
//  DashboardVC.swift
//  IPH_Decornt
//
//  Created by xx on 20/08/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class DashboardVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var tblHome: UITableView!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet var viewIssue: UIView!
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var NoOfResultCell = NSMutableArray()
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Method
    
    //---------------------------------------------
    
    func setUpView() {
        self.setTitle(title: "Decornt")
        self.setLeftBar(isMenuRequired: true)
        self.setRightData(isSearchBtnRequired: false)
        
        //self.viewTop.backgroundColor = self.navigationController?.navigationBar.barTintColor
        
        self.tblHome.tableFooterView = self.viewIssue
        
        let nibName = UINib(nibName: "HeaderHomeCell", bundle: nil)
        self.tblHome.register(nibName, forCellReuseIdentifier: "HeaderHomeCell")
        
        let nibName1 = UINib(nibName: "HomeCategory1Cell", bundle: nil)
        self.tblHome .register(nibName1, forCellReuseIdentifier: "HomeCategory1Cell")
        
        let nibName2 = UINib(nibName: "HomeCategory2Cell", bundle: nil)
        self.tblHome .register(nibName2, forCellReuseIdentifier: "HomeCategory2Cell")
        
        let nibName3 = UINib(nibName: "ProductTableCell", bundle: nil)
        self.tblHome .register(nibName3, forCellReuseIdentifier: "ProductTableCell")
        
        let nibName4 = UINib(nibName: "BottomHomeCell", bundle: nil)
        self.tblHome .register(nibName4, forCellReuseIdentifier: "BottomHomeCell")
        
        
        let nibName5 = UINib(nibName: "AdBottomHomeCell", bundle: nil)
        self.tblHome .register(nibName5, forCellReuseIdentifier: "AdBottomHomeCell")
        
        let nibName6 = UINib(nibName: "BannerCell", bundle: nil)
        self.tblHome .register(nibName6, forCellReuseIdentifier: "BannerCell")
        
        let nibName7 = UINib(nibName: "BrandCell", bundle: nil)
        self.tblHome .register(nibName7, forCellReuseIdentifier: "BrandCell")
        
        //self.perform(#selector(ProductHomeViewController.GetHomeResult), with: nil, afterDelay: 1.0)
        //self.callWSDashboard()
        
        if let cartCount = UserDefaults.UserData.object(forKey: .cartCount) as? String {
            self.lblCount.text = cartCount
        }
    }
    
    //---------------------------------------------
    
    //MARK:- Action Method
    
    //---------------------------------------------
    
    @IBAction func btnContactTapped(_ sender: Any) {
        let contactVC = AppStoryboard.ContactUs.viewController(viewControllerClass: ContactUsVC.self)
        self.navigationController?.pushViewController(contactVC, animated: true)
    }
    
    @IBAction func btnOfferTapped(_ sender: Any) {
        let NotificaionVC = AppStoryboard.Notification.viewController(viewControllerClass: NotificationVC.self)
       
        NotificaionVC.defaultName = "Offers"
        self.navigationController?.pushViewController(NotificaionVC, animated: true)
    }
    
    @IBAction func btnCartTapped(_ sender: Any) {
        let myCartVC = AppStoryboard.MyCart.viewController(viewControllerClass: MyCartVC.self)
        self.navigationController?.pushViewController(myCartVC, animated: true)
    }
    
    
    @IBAction func btnNotificationTapped(_ sender: Any) {
        let NotificaionVC = AppStoryboard.Notification.viewController(viewControllerClass: NotificationVC.self)
        self.navigationController?.pushViewController(NotificaionVC, animated: true)
    }
    
    @IBAction func btnCategoryTapped(_ sender: Any) {
        let categoryVC = AppStoryboard.Product.viewController(viewControllerClass: CategoryVC.self)
        
        self.navigationController?.pushViewController(categoryVC, animated: true)
    }
    
    @IBAction func btnSearchTapped(_ sender: Any) {
        let searchVC = AppStoryboard.Dashboard.viewController(viewControllerClass: SearchVC.self)
        
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    //---------------------------------------------
    
    //MARK:- CallWS Method
    
    //---------------------------------------------
    
    func callWSDashboard() {
        
        //https://www.decornt.com/mapp/index.php?view=dashboard_bottom&custID=MTg=
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        let apiName = "dashboard&custID=\(strCustId)&itoken=sdasd&ideviceID=asds"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            
            
            if success {
                
                var productData:[String : Any] = response as! [String : Any]
                
                let apiBottomName = "dashboard_bottom&custID=\(strCustId)"
                
                DataManager.sharedInstance.getRequestApi(apiName: apiBottomName) { (response, success) in
                    
                    AppDelegate.sharedInstance.stopLoadingIndicator()
                    if let resultList = productData["banners"] as? NSArray {
                        if let cell = self.tblHome.dequeueReusableCell(withIdentifier: "BottomHomeCell") as? BottomHomeCell {
                            cell.ResponseResult = resultList
                            self.NoOfResultCell.add(cell)
                            
                            DispatchQueue.main.async
                                {
                                    self.tblHome.delegate = self
                                    self.tblHome.dataSource = self
                                    self.tblHome.reloadData()
                            }
                        }
                        
                        if let resultList = productData["new_product"] as? NSArray {
                            if let cell = self.tblHome.dequeueReusableCell(withIdentifier: "ProductTableCell") as? ProductTableCell {
                                cell.delegate = self
                                cell.productTitleLabel.text = "New Arrivals"
                                cell.ResponseResult = resultList
                                cell.btnViewAll.isHidden = true
                                self.NoOfResultCell.add(cell)
                                
                                DispatchQueue.main.async
                                    {
                                        self.tblHome.delegate = self
                                        self.tblHome.dataSource = self
                                        self.tblHome.reloadData()
                                }
                            }
                            
                            
                        }
                        
                        if let resultList = productData["today_product_list"] as? NSArray {
                            if let cell = self.tblHome.dequeueReusableCell(withIdentifier: "ProductTableCell") as? ProductTableCell {
                                cell.delegate = self
                                cell.productTitleLabel.text = "Today's Deal"
                                cell.ResponseResult = resultList
                                cell.btnViewAll.isHidden = true
                                self.NoOfResultCell.add(cell)
                                
                                DispatchQueue.main.async
                                    {
                                        self.tblHome.delegate = self
                                        self.tblHome.dataSource = self
                                        self.tblHome.reloadData()
                                }
                            }
                            
                            
                        }
                        
                        if let resultList = productData["best_product"] as? NSArray {
                            if let cell = self.tblHome.dequeueReusableCell(withIdentifier: "ProductTableCell") as? ProductTableCell {
                                cell.delegate = self
                                cell.productTitleLabel.text = "Best Selling"
                                cell.ResponseResult = resultList
                                cell.btnViewAll.isHidden = true
                                self.NoOfResultCell.add(cell)
                                
                                DispatchQueue.main.async
                                    {
                                        self.tblHome.delegate = self
                                        self.tblHome.dataSource = self
                                        self.tblHome.reloadData()
                                }
                            }
                            
                            
                        }
                     }
                    
                    let productResultData:[String : Any] = response as! [String : Any]
                    
                    if let resultList = productResultData["brands"] as? NSArray {
                        if let cell = self.tblHome.dequeueReusableCell(withIdentifier: "BrandCell") as? BrandCell {
                            
                            cell.ResponseResult = resultList
                            cell.delegate = self
                            self.NoOfResultCell.add(cell)
                            
                            DispatchQueue.main.async
                                {
                                    self.tblHome.delegate = self
                                    self.tblHome.dataSource = self
                                    self.tblHome.reloadData()
                            }
                        }
                        
                        
                    }
                    self.setBannerData(productData: productResultData, bannerName: "cat_banner1")
                    self.setProductData(productData: productResultData, strCategory: "cat1", strCatId: "Mzc=", strProduct: "cat1_product")
                    
                    self.setBannerData(productData: productResultData, bannerName: "cat_banner2")
                    self.setProductData(productData: productResultData, strCategory: "cat2", strCatId: "NDA=", strProduct: "cat2_product")
                    
                    self.setBannerData(productData: productResultData, bannerName: "cat_banner3")
                    self.setProductData(productData: productResultData, strCategory: "cat3", strCatId: "Mzg=", strProduct: "cat3_product")
                    
                    self.setBannerData(productData: productResultData, bannerName: "cat_banner4")
                    self.setProductData(productData: productResultData, strCategory: "cat4", strCatId: "NDQ=", strProduct: "cat4_product")
                    
                    self.setBannerData(productData: productResultData, bannerName: "cat_banner5")
                   self.setProductData(productData: productResultData, strCategory: "cat5", strCatId: "NDE=", strProduct: "cat5_product")
                    
                    self.setBannerData(productData: productResultData, bannerName: "cat_banner6")
                    self.setProductData(productData: productResultData, strCategory: "cat6", strCatId: "Mzk=", strProduct: "cat6_product")
                }
                
            } else {
                self.showAlert(App.AppName, message: response as? String)
            }
        }
        
    }
    
    
    func setProductData(productData : [String : Any], strCategory : String, strCatId : String, strProduct : String) {
        
        if let resultList = productData[strProduct] as? NSArray {
            if let cell = self.tblHome.dequeueReusableCell(withIdentifier: "ProductTableCell") as? ProductTableCell {
                cell.delegate = self
                if let resultName = productData[strCategory] as? String {
                    cell.productTitleLabel.text = resultName
                }
                
                
                cell.ResponseResult = resultList
                cell.catId = strCatId
                cell.productDelegate = self
                self.NoOfResultCell.add(cell)
            
                DispatchQueue.main.async
                    {
                         self.tblHome.delegate = self
                         self.tblHome.dataSource = self
                        self.tblHome.reloadData()
                }
            }
        }
    }
    
    func setBannerData(productData : [String : Any], bannerName : String) {
        
        if let resultImg = productData[bannerName] as? String {
            if let cell = self.tblHome.dequeueReusableCell(withIdentifier: "BannerCell") as? BannerCell {
                cell.bannerStr = resultImg
                cell.setImageData()
                self.NoOfResultCell.add(cell)
                
                DispatchQueue.main.async
                    {
                        self.tblHome.delegate = self
                        self.tblHome.dataSource = self
                        self.tblHome.reloadData()
                }
            }
        }
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}

extension DashboardVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.NoOfResultCell.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.NoOfResultCell.count > indexPath.row {
            
            if let _ = self.NoOfResultCell.object(at: indexPath.row) as? HeaderHomeCell {
                return 150
            } else if let _ = self.NoOfResultCell.object(at: indexPath.row) as? ProductTableCell {
                return 300
            }else if let _ = self.NoOfResultCell.object(at: indexPath.row) as? BottomHomeCell {
                return 150
            }else if let _ = self.NoOfResultCell.object(at: indexPath.row) as? AdBottomHomeCell {
                return 180
            } else if let _ = self.NoOfResultCell.object(at: indexPath.row) as? BannerCell {
                return 160
            } else if let _ = self.NoOfResultCell.object(at: indexPath.row) as? BrandCell {
                return 175
            }
        }
        
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.NoOfResultCell.count > indexPath.row {
            
            if let cell = self.NoOfResultCell.object(at: indexPath.row) as? HeaderHomeCell {
                return cell
            } else if let cell = self.NoOfResultCell.object(at: indexPath.row) as? ProductTableCell {
                return cell
            }else if let cell = self.NoOfResultCell.object(at: indexPath.row) as? BottomHomeCell {
                return cell
            }else if let cell = self.NoOfResultCell.object(at: indexPath.row) as? AdBottomHomeCell {
                return cell
            }else if let cell = self.NoOfResultCell.object(at: indexPath.row) as? BannerCell {
                return cell
            }else if let cell = self.NoOfResultCell.object(at: indexPath.row) as? BrandCell {
                return cell
            }
            
        }
        return UITableViewCell()
    }
    
}

extension DashboardVC: DeskBoardCategoryDelegate {
    
    func didLoadCategoryResult(data: NSDictionary) {
        print(data)
        
        if let subcat = data.object(forKey: "subcat") as? String {
            if subcat.caseInsensitiveCompare("yes") == .orderedSame {
                let viewController = AppStoryboard.Product.viewController(viewControllerClass: CategoryVC.self)
                viewController.homeCategoryResult = data
                self.navigationController?.pushViewController(viewController, animated: true)
                return
            }
        }
        if data.object(forKey: "catID") != nil {
            let objRoot = AppStoryboard.Product.viewController(viewControllerClass: ProductListVC.self)
            objRoot.categoryResultData = data
            self.navigationController?.pushViewController(objRoot, animated: true)
        }else if data.object(forKey: "productID") != nil {
            let objRoot = AppStoryboard.Product.viewController(viewControllerClass: ProductDetailsVC.self)
            objRoot.productResult = data
            self.navigationController?.pushViewController(objRoot, animated: true)
        }
    }
}

extension DashboardVC : ViewProductDelegate {
    
    func viewAllTapped(catId: String, name : String) {
        
        let data = NSMutableDictionary()
        data.setObject(catId, forKey: "catID" as NSCopying)
        data.setObject(name, forKey: "name" as NSCopying)
        let objRoot = AppStoryboard.Product.viewController(viewControllerClass: ProductListVC.self)
        objRoot.categoryResultData = data
        self.navigationController?.pushViewController(objRoot, animated: true)
    }
}

