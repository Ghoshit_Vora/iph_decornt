//
//  LoginVC.swift
//  IPH_Decornt
//
//  Created by xx on 20/08/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class LoginVC: BaseVC {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var txtMobile: UITextField!
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Method
    
    //---------------------------------------------
    
    func setUpView() {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //---------------------------------------------
    
    //MARK:- Action Methods
    
    //---------------------------------------------
    
    @IBAction func btnLoginTapped(_ sender: Any) {
        
        //let vc = AppStoryboard.Dashboard.viewController(viewControllerClass: DashboardVC.self)
        //AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: vc)
        let validator = Validator()
        
        validator.registerField(self.txtMobile, rule: [RequiredRule(message: "Please enter mobile number"), PhoneNumberRule(message: "Please enter valid mobile number") ])
        
        validator.validate({
            
            self.callVerificationWS()
        }) { (arrValidationError) in
            
            if let firstError = arrValidationError.first {
                self.showAlertWithMessage(firstError.errorMessage)
            }
        }
        
    }
    
    //---------------------------------------------
    
    //MARK:- WS Methods
    
    //---------------------------------------------
    
    func callVerificationWS() {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        //https://www.decornt.com/mapp/index.php?view=login2&sphone=9510069163
        let apiName = "login2&sphone=\(self.txtMobile.text!)"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                let vc = AppStoryboard.Authotication.viewController(viewControllerClass: OTPVerifyVC.self)
               
                vc.strPhone = "+91 \(self.txtMobile.text!)"
                
                var loginData:[String : Any] = response as! [String : Any]
                UserDefaults.UserData.set(loginData["ID"] as AnyObject, forKey: UserDefaults.UserData.ObjectDefaultKey.userId)
                
                UserDefaults.UserData.set(loginData["phone"] as AnyObject, forKey: UserDefaults.UserData.ObjectDefaultKey.phone)
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}
