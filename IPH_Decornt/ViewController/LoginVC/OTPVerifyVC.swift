//
//  OTPVerifyVC.swift
//  IPH_Decornt
//
//  Created by xx on 25/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class OTPVerifyVC: UIViewController {

    //---------------------------------------------
    
    //MARK:- Properties For Outlet
    
    //---------------------------------------------
    
    @IBOutlet weak var txtOTP: UITextField!
    @IBOutlet weak var lblMobileNo: UILabel!
    
    //---------------------------------------------
    
    //MARK:- Properties For Local
    
    //---------------------------------------------
    
    var strPhone = String()
    
    //---------------------------------------------
    
    //MARK:- Memoery Management Method
    
    //---------------------------------------------
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //---------------------------------------------
    
    //MARK:- Custom Method
    
    //---------------------------------------------
    
    func setUpView() {
        
        self.navigationController?.navigationBar.isHidden = true
    
        self.lblMobileNo.text = strPhone
    }
    
    //---------------------------------------------
    
    //MARK:- Action Methods
    
    //---------------------------------------------
    
    @IBAction func btnVerifyOTPTapped(_ sender: Any) {
        
        if self.txtOTP.text != "" {
            
            self.callOTPVerificationWS()
        }
    }
    
    @IBAction func btnResendTapped(_ sender: Any) {
    }
    
    
    @IBAction func btnContactUsTapped(_ sender: Any) {
    }
    
    func callOTPVerificationWS() {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        //https://www.decornt.com/mapp/index.php?view=otp_verify&type=LOGIN&custID=MTg=&otp=3061
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        let apiName = "otp_verify&type=LOGIN&custID=\(strCustId)&otp=\(self.txtOTP.text!)"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
               
               var loginData:[String : Any] = response as! [String : Any]
                
                if let arrCustomer = loginData["customer_detail"] as? [[String : Any]] {
                    
                    let dictData = arrCustomer[0]
                    
                    if let name = dictData["name"] as? String {
                       UserDefaults.UserData.set(name as AnyObject, forKey: UserDefaults.UserData.ObjectDefaultKey.name)
                    }
                    
                    if let email = dictData["email"] as? String {
                        UserDefaults.UserData.set(email as AnyObject, forKey: UserDefaults.UserData.ObjectDefaultKey.email)
                    }
                }
                
                 UserDefaults.UserData.set(loginData["cart_count"] as AnyObject, forKey: UserDefaults.UserData.ObjectDefaultKey.cartCount)
                
                UserDefaults.Account.set(true, forKey: UserDefaults.Account.BoolDefaultKey.isUserLoggedIn)
                
                let vc = AppStoryboard.Dashboard.viewController(viewControllerClass: DashboardVC.self)
                AppDelegate.sharedInstance.SetUpViewInSideControllerView(rootVC: vc)
            }
            
        }
    }
    
    func callResendOTPWS() {
        
        if AppDelegate.sharedInstance.isConnectedToNetwork() == false {
            
            return
        }
        
        //https://www.decornt.com/mapp/index.php?view=otp_verify&type=LOGIN&custID=MjA=&page=resend_otp
        
        var strCustId = String()
        
        if let id = UserDefaults.UserData.object(forKey: .userId) as? String {
            strCustId = id
        }
        
        let apiName = "otp_verify&type=LOGIN&custID=\(strCustId)&page=resend_otp"
        
        AppDelegate.sharedInstance.startLoadingIndicator()
        DataManager.sharedInstance.getRequestApi(apiName: apiName) { (response, success) in
            
            AppDelegate.sharedInstance.stopLoadingIndicator()
            
            if success {
                
            }
            
        }
    }
    
    //---------------------------------------------
    
    //MARK:- View Life Cycle Methods
    
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
    }
    
    //---------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //---------------------------------------------

}
