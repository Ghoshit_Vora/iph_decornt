//
//  DataManager.swift
//  SKA
//
//  Created by aipxperts on 01/06/18.
//  Copyright © 2018 aipxperts. All rights reserved.
//

import Foundation
import Alamofire

struct WSUrl {
    //static let stag = "http://ska.aipxperts.com/api/v1/"
    static let live = "https://www.decornt.com/mapp/index.php?view="
}

struct endPoints {
    static let login = "login"
}

typealias responseHandler = (Any , Bool) -> Void

class DataManager {
    
    static let sharedInstance = DataManager()
    var Alamofiremanager: SessionManager!
    
    //MARK: - Get Api Method
    
    func getRequestApi(apiName : String,completionHandler: responseHandler!) {
        
        URLCache.shared.removeAllCachedResponses()
        
        
        let APIMethod: HTTPMethod = HTTPMethod.get
        
        let APIURL =  URL(string: "\(WSUrl.live)\(apiName)")
        
        let dict:[String : Any] = [:]
        
        Alamofire.request(APIURL!, method: APIMethod, parameters: dict, encoding: URLEncoding.default, headers: nil).responseString(completionHandler: { (ResponseStriing:DataResponse<String>) in
            print(ResponseStriing)
        })
            .responseJSON { (response) in
                
                if let JSON = response.result.value {
                    
                    let (JSON, success) = self.checkResponse(webservice: apiName, dict: JSON as! Dictionary<String, AnyObject>)
                    
                    if(success == false) {

                            completionHandler((JSON as! String), false)

                    } else {
                    
                        completionHandler(JSON, true)
                    
                    }
                    
                } else {
                    
                    completionHandler("Network Error!", false)
                
                }
        }
    }
    
    func checkResponse(webservice: String, dict : [String:AnyObject]) -> (AnyObject, Bool) {
        
            if dict["msgcode"] as! String != "1" {
                return (dict as AnyObject, true)
            } else {
                return (dict["message"]!, false)
            }
        
    }
    
}
