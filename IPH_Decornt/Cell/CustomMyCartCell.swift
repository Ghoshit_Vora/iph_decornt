//
//  CustomMyCartCell.swift
//  IPH_Decornt
//
//  Created by xx on 16/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class CustomMyCartCell: UITableViewCell {

    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblMrp: UILabel!
    @IBOutlet weak var btnMoveToWishList: UIButton!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var btnQty: UIButton!
    @IBOutlet weak var imgProduct: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpData(dictData : [String:Any]) {
    
        if let name = dictData["name"] as? String {
            self.lblProductName.text = name
        }
        
        if let price = dictData["price"] as? String {
            self.lblPrice.text = "₹ " + price
        }
        
        if let model = dictData["model"] as? String {
            self.lblQty.text = model
        }
        
        if let mrp  = dictData["mrp"] as? String {
            self.lblMrp.text = "₹ " + mrp
            let attributedString = NSMutableAttributedString(string: self.lblMrp.text!)
            attributedString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: 2, range: NSMakeRange(0, attributedString.length))
            self.lblMrp.attributedText = attributedString
        
            
        }
        
        if let imgProduct  = dictData["image"] as? String {
            if let imgUrl = URL(string: imgProduct) {
                self.imgProduct.sd_setImage(with: imgUrl, completed: nil)
            }
        }
        
        if let qty = dictData["quantity"] as? String {
            self.btnQty.setTitle("Qty : \(qty)", for: .normal)
        }
    }

}
