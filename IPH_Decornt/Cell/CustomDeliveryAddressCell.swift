//
//  CustomDeliveryAddressCell.swift
//  IPH_Decornt
//
//  Created by xx on 30/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class CustomDeliveryAddressCell: UITableViewCell {

    @IBOutlet weak var btnRadio: UIButton!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblMobile: UILabel!
    
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       self.setCardView(view: self.btnEdit)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(dict : [String : Any], seletedAddressId : String) {
        
        if let name = dict["name"] as? String {
            self.lblName.text = name
        }
        
        if let contact = dict["phone"] as? String {
            self.lblMobile.text = contact
        }
        
        var strAddress = String()
        
        if let address1 = dict["address1"] as? String {
            strAddress = address1
        }
        
        if let address2 = dict["address2"] as? String {
            strAddress = strAddress + ", " + address2
        }
        
        if let area = dict["area"] as? String {
            strAddress = strAddress + ",\n" + area
        }
        
        if let city = dict["city"] as? String {
            strAddress = strAddress + ", " + city
        }
        
        if let state = dict["state"] as? String {
            strAddress = strAddress + ", " + state
        }
        
        if let pincode = dict["pincode"] as? String {
            strAddress = strAddress + ", " + pincode
        }
        
        self.lblAddress.text = strAddress
        
        if let addressId = dict["addID"] as? String {
            if addressId == seletedAddressId {
                self.btnRadio.isSelected = true
                self.btnEdit.isHidden = false
            } else {
                self.btnRadio.isSelected = false
                self.btnEdit.isHidden = true
            }
        } else {
            self.btnRadio.isSelected = false
            self.btnEdit.isHidden = true
        }
    
        self.btnEdit.dropShadow()
    }
    

}
