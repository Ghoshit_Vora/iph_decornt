//
//  CustomPriceDetailCell.swift
//  IPH_Decornt
//
//  Created by xx on 27/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class CustomPriceDetailCell: UITableViewCell {

    @IBOutlet weak var lblTotalItem: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPayblePrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
