//
//  MyAddreessCellTableViewCell.swift
//  IPH_Decornt
//
//  Created by xx on 08/09/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class MyAddreessCellTableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var btnAddressMenu: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:-Custom Method
    
    func setUpData(dict : [String:Any]) {
        
        if let name = dict["name"] as? String {
            self.lblName.text = name
        }
        
        if let contact = dict["phone"] as? String {
            self.lblContact.text = contact
        }
        
        var strAddress = String()
        
        if let address1 = dict["address1"] as? String {
            strAddress = address1
        }
        
        if let address2 = dict["address2"] as? String {
            strAddress = strAddress + ", " + address2
        }
        
        if let area = dict["area"] as? String {
            strAddress = strAddress + ",\n" + area
        }
        
        if let city = dict["city"] as? String {
            strAddress = strAddress + ", " + city
        }
        
        if let state = dict["state"] as? String {
            strAddress = strAddress + ", " + state
        }
        
        if let pincode = dict["pincode"] as? String {
            strAddress = strAddress + ", " + pincode
        }
        
        self.lblAddress.text = strAddress
        
    }

}
