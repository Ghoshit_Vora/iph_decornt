//
//  CustomNotificationCell.swift
//  IPH_Decornt
//
//  Created by xx on 24/08/18.
//  Copyright © 2018 xx. All rights reserved.
//

import UIKit

class CustomNotificationCell: UITableViewCell {

    @IBOutlet weak var imgNotification: UIImageView!
    @IBOutlet weak var lblNotificationName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
